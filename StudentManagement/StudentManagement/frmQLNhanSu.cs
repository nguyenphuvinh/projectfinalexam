﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmQLNhanSu : Form
    {
        public frmQLNhanSu()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {

            try
            {
                gridQLNS.Rows.Clear();
                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "SELECT P.ID, P.TEN, P.NGAY_SINH, P.DIA_CHI, " +
                                    " B.TEN as BO_MON, K.TEN AS KHOA, L.LUONG, L.PHU_CAP " +
                                    " FROM ADMIN.PEOPLE P " +
                                    " left JOIN ADMIN.BO_MON B ON B.ID = P.BO_MON " +
                                    " left JOIN ADMIN.KHOA K ON K.ID = P.KHOA " +
                                    " inner JOIN ADMIN.LUONG L ON L.NHAN_VIEN = P.ID order by p.id";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    gridQLNS.Rows.Add(!dr.IsDBNull(0) ? dr.GetValue(0) : "NULL",
                         !dr.IsDBNull(1) ? dr.GetString(1) : "NULL",
                        !dr.IsDBNull(2) ? dr.GetDateTime(2).ToString("yyyy/MM/dd") : "NULL",
                        !dr.IsDBNull(3) ? dr.GetString(3) : "NULL",
                        !dr.IsDBNull(4) ? dr.GetString(4) : "NULL",
                        !dr.IsDBNull(5) ? dr.GetString(5) : "NULL",
                        !dr.IsDBNull(6) ? dr.GetValue(6) : "NULL",
                        !dr.IsDBNull(7) ? dr.GetValue(7) : "NULL",
                        "Update"
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridQLNS_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex == gridQLNS.Columns[8].Index && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.gridQLNS.Rows[e.RowIndex];

                    DatabaseConnect.Cmd.CommandText = "UPDATE admin.PEOPLE SET " +
                        " NGAY_SINH = TO_DATE('" +
                    row.Cells[2].Value.ToString() + "', 'yyyy/mm/dd')," +
                        " DIA_CHI = '" +
                    row.Cells[3].Value.ToString() +
                        "', TEN = '" +
                    row.Cells[1].Value.ToString() + "' " +
                        " WHERE ID = " + row.Cells[0].Value.ToString();
                    int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();

                    DatabaseConnect.Cmd.CommandText = "UPDATE admin.LUONG SET " +
                        " LUONG = " +
                    row.Cells[6].Value.ToString() + " , " +
                        " PHU_CAP = " +
                    row.Cells[7].Value.ToString() +
                        " WHERE NHAN_VIEN = " + row.Cells[0].Value.ToString();
                    rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();

                    if (rowsUpdated != 0)
                        MessageBox.Show("Thành công!");

                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

               MessageBox.Show("Không thể cập nhật do lỗi hệ thống.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmThemNhanVien themNV = new frmThemNhanVien();
            themNV.Show();
        }

        private void gridQLNS_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (gridQLNS.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
            {
               e.Cancel = true;
            }
        }

        private void gridQLNS_Validating(object sender, CancelEventArgs e)
        {

        }

        private void frmQLNhanSu_FormClosed(object sender, FormClosedEventArgs e)
        {
            DatabaseConnect.FrmLogin.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loadData();
        }
    }
}
