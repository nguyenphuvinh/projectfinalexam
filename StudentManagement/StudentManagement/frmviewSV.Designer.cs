﻿namespace StudentManagement
{
    partial class frmviewSV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridSV = new System.Windows.Forms.DataGridView();
            this.masv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tensv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridSV)).BeginInit();
            this.SuspendLayout();
            // 
            // gridSV
            // 
            this.gridSV.AllowUserToAddRows = false;
            this.gridSV.AllowUserToDeleteRows = false;
            this.gridSV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.masv,
            this.tensv});
            this.gridSV.Location = new System.Drawing.Point(-1, 0);
            this.gridSV.Name = "gridSV";
            this.gridSV.ReadOnly = true;
            this.gridSV.Size = new System.Drawing.Size(363, 277);
            this.gridSV.TabIndex = 0;
            // 
            // masv
            // 
            this.masv.HeaderText = "Masv";
            this.masv.Name = "masv";
            this.masv.ReadOnly = true;
            // 
            // tensv
            // 
            this.tensv.HeaderText = "Tên Sinh Viên";
            this.tensv.Name = "tensv";
            this.tensv.ReadOnly = true;
            // 
            // frmviewSV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 273);
            this.Controls.Add(this.gridSV);
            this.Name = "frmviewSV";
            this.Text = "Danh Sách Sinh Viên";
            ((System.ComponentModel.ISupportInitialize)(this.gridSV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn masv;
        private System.Windows.Forms.DataGridViewTextBoxColumn tensv;
    }
}