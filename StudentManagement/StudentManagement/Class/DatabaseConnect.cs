﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types; 

namespace StudentManagement.Class
{
    static class DatabaseConnect
    {
        private static string oradb;

        public static string Oradb
        {
            get { return oradb; }
            set { oradb = value; }
        }
        private static OracleConnection conn;

        public static OracleConnection Conn
        {
            get { return conn; }
            set { conn = value; }
        }
        private static OracleCommand cmd = new OracleCommand();

        public static OracleCommand Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }
        private static frmLogin frmLogin;

        public static frmLogin FrmLogin
        {
            get { return DatabaseConnect.frmLogin; }
            set { DatabaseConnect.frmLogin = value; }
        }
    }
}
