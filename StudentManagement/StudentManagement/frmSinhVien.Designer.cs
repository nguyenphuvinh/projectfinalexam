﻿namespace StudentManagement
{
    partial class frmSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlSinhVien = new System.Windows.Forms.TabControl();
            this.tabThongTinSV = new System.Windows.Forms.TabPage();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.butUpdate = new System.Windows.Forms.Button();
            this.txtNgaySinh = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbNganh = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbTenSV = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbMSSV = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabXemDiemSV = new System.Windows.Forms.TabPage();
            this.but_SV_XD = new System.Windows.Forms.Button();
            this.grid_SV_XemDiem = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comb_SV_XD_NamHoc = new System.Windows.Forms.ComboBox();
            this.comb_SV_XD_HocKi = new System.Windows.Forms.ComboBox();
            this.tabDangKiMH = new System.Windows.Forms.TabPage();
            this.but_SV_DKMH = new System.Windows.Forms.Button();
            this.grid_SV_MHDK = new System.Windows.Forms.DataGridView();
            this.txtTenMHDK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ngayhh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONHOCDK_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkHuyDK = new System.Windows.Forms.DataGridViewLinkColumn();
            this.label14 = new System.Windows.Forms.Label();
            this.grid_SV_DKMH = new System.Windows.Forms.DataGridView();
            this.txtTenMH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtGiaovien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monhocid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSoLuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ngayhhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDangKi = new System.Windows.Forms.DataGridViewLinkColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.comb_SV_DKMH_NamHoc = new System.Windows.Forms.ComboBox();
            this.comb_SV_DKMH_Hocki = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControlSinhVien.SuspendLayout();
            this.tabThongTinSV.SuspendLayout();
            this.tabXemDiemSV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_XemDiem)).BeginInit();
            this.tabDangKiMH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_MHDK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_DKMH)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlSinhVien
            // 
            this.tabControlSinhVien.Controls.Add(this.tabThongTinSV);
            this.tabControlSinhVien.Controls.Add(this.tabXemDiemSV);
            this.tabControlSinhVien.Controls.Add(this.tabDangKiMH);
            this.tabControlSinhVien.Controls.Add(this.tabPage1);
            this.tabControlSinhVien.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlSinhVien.Location = new System.Drawing.Point(-3, -2);
            this.tabControlSinhVien.Name = "tabControlSinhVien";
            this.tabControlSinhVien.SelectedIndex = 0;
            this.tabControlSinhVien.Size = new System.Drawing.Size(785, 487);
            this.tabControlSinhVien.TabIndex = 0;
            this.tabControlSinhVien.SelectedIndexChanged += new System.EventHandler(this.tabControlSinhVien_SelectedIndexChanged);
            // 
            // tabThongTinSV
            // 
            this.tabThongTinSV.BackColor = System.Drawing.SystemColors.Control;
            this.tabThongTinSV.Controls.Add(this.txtTen);
            this.tabThongTinSV.Controls.Add(this.butUpdate);
            this.tabThongTinSV.Controls.Add(this.txtNgaySinh);
            this.tabThongTinSV.Controls.Add(this.label7);
            this.tabThongTinSV.Controls.Add(this.txtDiaChi);
            this.tabThongTinSV.Controls.Add(this.label6);
            this.tabThongTinSV.Controls.Add(this.lbNganh);
            this.tabThongTinSV.Controls.Add(this.label8);
            this.tabThongTinSV.Controls.Add(this.lbTenSV);
            this.tabThongTinSV.Controls.Add(this.label10);
            this.tabThongTinSV.Controls.Add(this.lbMSSV);
            this.tabThongTinSV.Controls.Add(this.label3);
            this.tabThongTinSV.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabThongTinSV.Location = new System.Drawing.Point(4, 30);
            this.tabThongTinSV.Name = "tabThongTinSV";
            this.tabThongTinSV.Padding = new System.Windows.Forms.Padding(3);
            this.tabThongTinSV.Size = new System.Drawing.Size(777, 453);
            this.tabThongTinSV.TabIndex = 0;
            this.tabThongTinSV.Text = "Thông tin";
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(136, 69);
            this.txtTen.Name = "txtTen";
            this.txtTen.ReadOnly = true;
            this.txtTen.Size = new System.Drawing.Size(175, 29);
            this.txtTen.TabIndex = 3;
            // 
            // butUpdate
            // 
            this.butUpdate.Location = new System.Drawing.Point(336, 217);
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Size = new System.Drawing.Size(85, 32);
            this.butUpdate.TabIndex = 2;
            this.butUpdate.Text = "Cập nhật";
            this.butUpdate.UseVisualStyleBackColor = true;
            this.butUpdate.Click += new System.EventHandler(this.butUpdate_Click);
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(136, 113);
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Size = new System.Drawing.Size(122, 29);
            this.txtNgaySinh.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 21);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ngày sinh:";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(136, 157);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(253, 29);
            this.txtDiaChi.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Địa chỉ:";
            // 
            // lbNganh
            // 
            this.lbNganh.AutoSize = true;
            this.lbNganh.Location = new System.Drawing.Point(442, 42);
            this.lbNganh.Name = "lbNganh";
            this.lbNganh.Size = new System.Drawing.Size(0, 21);
            this.lbNganh.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(385, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 21);
            this.label8.TabIndex = 0;
            this.label8.Text = "Khoa:";
            // 
            // lbTenSV
            // 
            this.lbTenSV.AutoSize = true;
            this.lbTenSV.Location = new System.Drawing.Point(132, 77);
            this.lbTenSV.Name = "lbTenSV";
            this.lbTenSV.Size = new System.Drawing.Size(0, 21);
            this.lbTenSV.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "Họ tên:";
            // 
            // lbMSSV
            // 
            this.lbMSSV.AutoSize = true;
            this.lbMSSV.Location = new System.Drawing.Point(132, 42);
            this.lbMSSV.Name = "lbMSSV";
            this.lbMSSV.Size = new System.Drawing.Size(0, 21);
            this.lbMSSV.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã số:";
            // 
            // tabXemDiemSV
            // 
            this.tabXemDiemSV.BackColor = System.Drawing.SystemColors.Control;
            this.tabXemDiemSV.Controls.Add(this.but_SV_XD);
            this.tabXemDiemSV.Controls.Add(this.grid_SV_XemDiem);
            this.tabXemDiemSV.Controls.Add(this.label9);
            this.tabXemDiemSV.Controls.Add(this.label5);
            this.tabXemDiemSV.Controls.Add(this.comb_SV_XD_NamHoc);
            this.tabXemDiemSV.Controls.Add(this.comb_SV_XD_HocKi);
            this.tabXemDiemSV.Location = new System.Drawing.Point(4, 30);
            this.tabXemDiemSV.Name = "tabXemDiemSV";
            this.tabXemDiemSV.Padding = new System.Windows.Forms.Padding(3);
            this.tabXemDiemSV.Size = new System.Drawing.Size(777, 453);
            this.tabXemDiemSV.TabIndex = 1;
            this.tabXemDiemSV.Text = "Xem Điểm";
            // 
            // but_SV_XD
            // 
            this.but_SV_XD.Location = new System.Drawing.Point(389, 47);
            this.but_SV_XD.Name = "but_SV_XD";
            this.but_SV_XD.Size = new System.Drawing.Size(75, 29);
            this.but_SV_XD.TabIndex = 2;
            this.but_SV_XD.Text = "Xem";
            this.but_SV_XD.UseVisualStyleBackColor = true;
            this.but_SV_XD.Click += new System.EventHandler(this.but_SV_XD_Click);
            // 
            // grid_SV_XemDiem
            // 
            this.grid_SV_XemDiem.AllowUserToAddRows = false;
            this.grid_SV_XemDiem.AllowUserToDeleteRows = false;
            this.grid_SV_XemDiem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid_SV_XemDiem.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.grid_SV_XemDiem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_SV_XemDiem.Location = new System.Drawing.Point(50, 113);
            this.grid_SV_XemDiem.Name = "grid_SV_XemDiem";
            this.grid_SV_XemDiem.ReadOnly = true;
            this.grid_SV_XemDiem.Size = new System.Drawing.Size(388, 178);
            this.grid_SV_XemDiem.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(200, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Năm học";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Học kì";
            // 
            // comb_SV_XD_NamHoc
            // 
            this.comb_SV_XD_NamHoc.FormattingEnabled = true;
            this.comb_SV_XD_NamHoc.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014"});
            this.comb_SV_XD_NamHoc.Location = new System.Drawing.Point(282, 47);
            this.comb_SV_XD_NamHoc.Name = "comb_SV_XD_NamHoc";
            this.comb_SV_XD_NamHoc.Size = new System.Drawing.Size(59, 29);
            this.comb_SV_XD_NamHoc.TabIndex = 1;
            // 
            // comb_SV_XD_HocKi
            // 
            this.comb_SV_XD_HocKi.FormattingEnabled = true;
            this.comb_SV_XD_HocKi.Items.AddRange(new object[] {
            "1",
            "2"});
            this.comb_SV_XD_HocKi.Location = new System.Drawing.Point(108, 47);
            this.comb_SV_XD_HocKi.Name = "comb_SV_XD_HocKi";
            this.comb_SV_XD_HocKi.Size = new System.Drawing.Size(39, 29);
            this.comb_SV_XD_HocKi.TabIndex = 0;
            // 
            // tabDangKiMH
            // 
            this.tabDangKiMH.BackColor = System.Drawing.SystemColors.Control;
            this.tabDangKiMH.Controls.Add(this.but_SV_DKMH);
            this.tabDangKiMH.Controls.Add(this.grid_SV_MHDK);
            this.tabDangKiMH.Controls.Add(this.label14);
            this.tabDangKiMH.Controls.Add(this.grid_SV_DKMH);
            this.tabDangKiMH.Controls.Add(this.label12);
            this.tabDangKiMH.Controls.Add(this.label13);
            this.tabDangKiMH.Controls.Add(this.comb_SV_DKMH_NamHoc);
            this.tabDangKiMH.Controls.Add(this.comb_SV_DKMH_Hocki);
            this.tabDangKiMH.Controls.Add(this.label11);
            this.tabDangKiMH.Location = new System.Drawing.Point(4, 30);
            this.tabDangKiMH.Name = "tabDangKiMH";
            this.tabDangKiMH.Padding = new System.Windows.Forms.Padding(3);
            this.tabDangKiMH.Size = new System.Drawing.Size(777, 453);
            this.tabDangKiMH.TabIndex = 2;
            this.tabDangKiMH.Text = "Đăng kí môn học";
            // 
            // but_SV_DKMH
            // 
            this.but_SV_DKMH.Location = new System.Drawing.Point(391, 18);
            this.but_SV_DKMH.Name = "but_SV_DKMH";
            this.but_SV_DKMH.Size = new System.Drawing.Size(75, 29);
            this.but_SV_DKMH.TabIndex = 9;
            this.but_SV_DKMH.Text = "Xem";
            this.but_SV_DKMH.UseVisualStyleBackColor = true;
            this.but_SV_DKMH.Click += new System.EventHandler(this.but_SV_DKMH_Click);
            // 
            // grid_SV_MHDK
            // 
            this.grid_SV_MHDK.AllowUserToAddRows = false;
            this.grid_SV_MHDK.AllowUserToDeleteRows = false;
            this.grid_SV_MHDK.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid_SV_MHDK.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grid_SV_MHDK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_SV_MHDK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtTenMHDK,
            this.ngayhh,
            this.MONHOCDK_ID,
            this.linkHuyDK});
            this.grid_SV_MHDK.Location = new System.Drawing.Point(30, 296);
            this.grid_SV_MHDK.Name = "grid_SV_MHDK";
            this.grid_SV_MHDK.ReadOnly = true;
            this.grid_SV_MHDK.Size = new System.Drawing.Size(621, 147);
            this.grid_SV_MHDK.TabIndex = 8;
            this.grid_SV_MHDK.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_SV_MHDK_CellClick);
            // 
            // txtTenMHDK
            // 
            this.txtTenMHDK.HeaderText = "Tên Môn Học";
            this.txtTenMHDK.Name = "txtTenMHDK";
            this.txtTenMHDK.ReadOnly = true;
            // 
            // ngayhh
            // 
            this.ngayhh.HeaderText = "Ngày Hết Hạn";
            this.ngayhh.Name = "ngayhh";
            this.ngayhh.ReadOnly = true;
            // 
            // MONHOCDK_ID
            // 
            this.MONHOCDK_ID.HeaderText = "Môn học ID";
            this.MONHOCDK_ID.Name = "MONHOCDK_ID";
            this.MONHOCDK_ID.ReadOnly = true;
            this.MONHOCDK_ID.Visible = false;
            // 
            // linkHuyDK
            // 
            this.linkHuyDK.HeaderText = "Hủy đăng kí";
            this.linkHuyDK.Name = "linkHuyDK";
            this.linkHuyDK.ReadOnly = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 264);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(195, 21);
            this.label14.TabIndex = 7;
            this.label14.Text = "Các môn học đã đăng kí";
            // 
            // grid_SV_DKMH
            // 
            this.grid_SV_DKMH.AllowUserToAddRows = false;
            this.grid_SV_DKMH.AllowUserToDeleteRows = false;
            this.grid_SV_DKMH.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid_SV_DKMH.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grid_SV_DKMH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_SV_DKMH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtTenMH,
            this.txtGiaovien,
            this.monhocid,
            this.txtSoLuong,
            this.ngayhhan,
            this.linkDangKi});
            this.grid_SV_DKMH.Location = new System.Drawing.Point(26, 92);
            this.grid_SV_DKMH.Name = "grid_SV_DKMH";
            this.grid_SV_DKMH.ReadOnly = true;
            this.grid_SV_DKMH.Size = new System.Drawing.Size(742, 150);
            this.grid_SV_DKMH.TabIndex = 6;
            this.grid_SV_DKMH.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_SV_DKMH_CellClick);
            // 
            // txtTenMH
            // 
            this.txtTenMH.HeaderText = "Tên Môn Học";
            this.txtTenMH.Name = "txtTenMH";
            this.txtTenMH.ReadOnly = true;
            // 
            // txtGiaovien
            // 
            this.txtGiaovien.HeaderText = "Giáo Viên";
            this.txtGiaovien.Name = "txtGiaovien";
            this.txtGiaovien.ReadOnly = true;
            // 
            // monhocid
            // 
            this.monhocid.HeaderText = "Môn học ID";
            this.monhocid.Name = "monhocid";
            this.monhocid.ReadOnly = true;
            this.monhocid.Visible = false;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.HeaderText = "Số lượng";
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.ReadOnly = true;
            // 
            // ngayhhan
            // 
            this.ngayhhan.HeaderText = "Ngày Hết Hạn";
            this.ngayhhan.Name = "ngayhhan";
            this.ngayhhan.ReadOnly = true;
            // 
            // linkDangKi
            // 
            this.linkDangKi.HeaderText = "Đăng Kí";
            this.linkDangKi.Name = "linkDangKi";
            this.linkDangKi.ReadOnly = true;
            this.linkDangKi.Text = "Đăng kí";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(213, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 21);
            this.label12.TabIndex = 4;
            this.label12.Text = "Năm học";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(59, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 21);
            this.label13.TabIndex = 5;
            this.label13.Text = "Học kì";
            // 
            // comb_SV_DKMH_NamHoc
            // 
            this.comb_SV_DKMH_NamHoc.FormattingEnabled = true;
            this.comb_SV_DKMH_NamHoc.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "2013"});
            this.comb_SV_DKMH_NamHoc.Location = new System.Drawing.Point(295, 20);
            this.comb_SV_DKMH_NamHoc.Name = "comb_SV_DKMH_NamHoc";
            this.comb_SV_DKMH_NamHoc.Size = new System.Drawing.Size(63, 29);
            this.comb_SV_DKMH_NamHoc.TabIndex = 2;
            this.comb_SV_DKMH_NamHoc.Text = "2015";
            // 
            // comb_SV_DKMH_Hocki
            // 
            this.comb_SV_DKMH_Hocki.FormattingEnabled = true;
            this.comb_SV_DKMH_Hocki.Items.AddRange(new object[] {
            "1",
            "2"});
            this.comb_SV_DKMH_Hocki.Location = new System.Drawing.Point(141, 17);
            this.comb_SV_DKMH_Hocki.Name = "comb_SV_DKMH_Hocki";
            this.comb_SV_DKMH_Hocki.Size = new System.Drawing.Size(41, 29);
            this.comb_SV_DKMH_Hocki.TabIndex = 3;
            this.comb_SV_DKMH_Hocki.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 21);
            this.label11.TabIndex = 0;
            this.label11.Text = "Đăng kí môn học";
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(777, 453);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Thông Báo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // frmSinhVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 483);
            this.Controls.Add(this.tabControlSinhVien);
            this.Location = new System.Drawing.Point(600, 400);
            this.Name = "frmSinhVien";
            this.Text = "Sinh Viên";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSinhVien_FormClosed);
            this.Load += new System.EventHandler(this.frmSinhVien_Load);
            this.tabControlSinhVien.ResumeLayout(false);
            this.tabThongTinSV.ResumeLayout(false);
            this.tabThongTinSV.PerformLayout();
            this.tabXemDiemSV.ResumeLayout(false);
            this.tabXemDiemSV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_XemDiem)).EndInit();
            this.tabDangKiMH.ResumeLayout(false);
            this.tabDangKiMH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_MHDK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_SV_DKMH)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlSinhVien;
        private System.Windows.Forms.TabPage tabThongTinSV;
        private System.Windows.Forms.TabPage tabXemDiemSV;
        private System.Windows.Forms.TabPage tabDangKiMH;
        private System.Windows.Forms.TextBox txtNgaySinh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbNganh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbTenSV;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbMSSV;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comb_SV_XD_NamHoc;
        private System.Windows.Forms.ComboBox comb_SV_XD_HocKi;
        private System.Windows.Forms.DataGridView grid_SV_XemDiem;
        private System.Windows.Forms.Button but_SV_XD;
        private System.Windows.Forms.Button but_SV_DKMH;
        private System.Windows.Forms.DataGridView grid_SV_MHDK;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView grid_SV_DKMH;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comb_SV_DKMH_NamHoc;
        private System.Windows.Forms.ComboBox comb_SV_DKMH_Hocki;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button butUpdate;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTenMHDK;
        private System.Windows.Forms.DataGridViewTextBoxColumn ngayhh;
        private System.Windows.Forms.DataGridViewTextBoxColumn MONHOCDK_ID;
        private System.Windows.Forms.DataGridViewLinkColumn linkHuyDK;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTenMH;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtGiaovien;
        private System.Windows.Forms.DataGridViewTextBoxColumn monhocid;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSoLuong;
        private System.Windows.Forms.DataGridViewTextBoxColumn ngayhhan;
        private System.Windows.Forms.DataGridViewLinkColumn linkDangKi;
        private System.Windows.Forms.TabPage tabPage1;
    }
}

