﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmThongBao : Form
    {
        string people_id;
        string chuc_vu;
        public frmThongBao(string id, string people_chuc_vu)
        {
            InitializeComponent();
            people_id = id;
            chuc_vu = people_chuc_vu;
            comb_ChucVu.Items.Clear();
            if (chuc_vu == "1")
                comb_ChucVu.Items.Add("Sinh Viên");
            else if (chuc_vu == "2")
            {
                comb_ChucVu.Items.Add("Sinh Viên");
                comb_ChucVu.Items.Add("Giáo Viên");
            }
            else if (chuc_vu == "7")
            {
                comb_ChucVu.Items.Add("Sinh Viên");
                comb_ChucVu.Items.Add("Giáo Viên");
                comb_ChucVu.Items.Add("Trưởng Khoa");
            }

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                gridDocThongBao.Rows.Clear();
                DatabaseConnect.Cmd.CommandText = "select p.TEN, c.ten, tb.noi_dung, tb.Ngay_thong_bao from admin.Thong_bao tb " +
                "left join admin.chuc_vu c on c.ID = tb.chuc_vu " +
                "left join ADMIN.PEOPLE p on tb.Nguoi_thong_bao = p.ID order by tb.Ngay_thong_bao asc";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    gridDocThongBao.Rows.Add(dr.GetString(0), dr.GetString(1), dr.GetString(2), dr.GetDateTime(3).ToShortDateString());
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnTaoTB_Click(object sender, EventArgs e)
        {
            try
            {
                string level, compartment, group, label;
                switch (comb_ChucVu.Text)
                {
                    case "Sinh Viên":
                        level = "sv"; break;
                    case "Giáo Viên":
                        level = "gv"; break;
                    case "Trưởng Khoa":
                        level = "tk"; break;
                    default:
                         level = "sv";break;
                }

                switch (combBoMon.Text)
                {
                    case "Hệ thống thông tin":
                        compartment = "httt"; break;
                    case "Khoa học máy tính":
                        compartment = "khmt"; break;
                    default:
                        compartment = "httt";break;
                }
                
                switch (combKhoa.Text)
                {
                    case "Công nghệ thông tin":
                        group = "it"; break;
                    case "Vật lý":
                        group = "vl"; break;
                    case "Hóa học":
                        group = "hh"; break;
                    default:
                        group = "it";break;
                }
                label = level + ":" + compartment + ":" + group;
                if(txtNoiDung.Text=="")
                {
                    MessageBox.Show("Chưa nhập nội dung.");
                    return;
                }
                DatabaseConnect.Cmd.CommandText = "Insert into admin.THONG_BAO (NGUOI_THONG_BAO, CHUC_VU,NOI_DUNG,NGAY_THONG_BAO, OLS_NUM) values (" +
                  people_id + ", " +
                   chuc_vu + ", '" +
                  txtNoiDung.Text + "', to_date(to_char(CURRENT_DATE,'yyyy/MM/dd'), 'yyyy/MM/dd'), char_to_label('XEM_THONG_BAO', '" +
                  label + "'))";
                int rowsInserted = DatabaseConnect.Cmd.ExecuteNonQuery();

                if (rowsInserted != 0)
                    MessageBox.Show("Thành công!");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
