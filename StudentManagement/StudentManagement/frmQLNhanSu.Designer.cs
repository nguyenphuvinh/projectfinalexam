﻿namespace StudentManagement
{
    partial class frmQLNhanSu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridQLNS = new System.Windows.Forms.DataGridView();
            this.maso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ten = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ngaysinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diachi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bomon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.khoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.luong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phucap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.update = new System.Windows.Forms.DataGridViewButtonColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridQLNS)).BeginInit();
            this.SuspendLayout();
            // 
            // gridQLNS
            // 
            this.gridQLNS.AllowUserToAddRows = false;
            this.gridQLNS.AllowUserToDeleteRows = false;
            this.gridQLNS.AllowUserToOrderColumns = true;
            this.gridQLNS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridQLNS.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridQLNS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridQLNS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maso,
            this.ten,
            this.ngaysinh,
            this.diachi,
            this.bomon,
            this.khoa,
            this.luong,
            this.phucap,
            this.update});
            this.gridQLNS.Location = new System.Drawing.Point(34, 14);
            this.gridQLNS.Margin = new System.Windows.Forms.Padding(5);
            this.gridQLNS.Name = "gridQLNS";
            this.gridQLNS.Size = new System.Drawing.Size(900, 412);
            this.gridQLNS.TabIndex = 0;
            this.gridQLNS.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridQLNS_CellClick);
            this.gridQLNS.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.gridQLNS_CellValidating);
            this.gridQLNS.Validating += new System.ComponentModel.CancelEventHandler(this.gridQLNS_Validating);
            // 
            // maso
            // 
            this.maso.HeaderText = "Mã Số";
            this.maso.Name = "maso";
            this.maso.ReadOnly = true;
            // 
            // ten
            // 
            this.ten.HeaderText = "Tên";
            this.ten.Name = "ten";
            // 
            // ngaysinh
            // 
            this.ngaysinh.HeaderText = "Ngày Sinh";
            this.ngaysinh.Name = "ngaysinh";
            // 
            // diachi
            // 
            this.diachi.HeaderText = "Địa chỉ";
            this.diachi.Name = "diachi";
            // 
            // bomon
            // 
            this.bomon.HeaderText = "Bộ Môn";
            this.bomon.Name = "bomon";
            this.bomon.ReadOnly = true;
            // 
            // khoa
            // 
            this.khoa.HeaderText = "Khoa";
            this.khoa.Name = "khoa";
            this.khoa.ReadOnly = true;
            // 
            // luong
            // 
            this.luong.HeaderText = "Lương";
            this.luong.Name = "luong";
            // 
            // phucap
            // 
            this.phucap.HeaderText = "Phụ Cấp";
            this.phucap.Name = "phucap";
            // 
            // update
            // 
            this.update.HeaderText = "Update";
            this.update.Name = "update";
            this.update.Text = "Update";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 437);
            this.button1.Margin = new System.Windows.Forms.Padding(5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 38);
            this.button1.TabIndex = 1;
            this.button1.Text = "Thêm nhân viên";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(868, 437);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 37);
            this.button2.TabIndex = 2;
            this.button2.Text = "F5";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmQLNhanSu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 485);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gridQLNS);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmQLNhanSu";
            this.Text = "Quản lý nhân sự";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmQLNhanSu_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gridQLNS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridQLNS;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn maso;
        private System.Windows.Forms.DataGridViewTextBoxColumn ten;
        private System.Windows.Forms.DataGridViewTextBoxColumn ngaysinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn diachi;
        private System.Windows.Forms.DataGridViewTextBoxColumn bomon;
        private System.Windows.Forms.DataGridViewTextBoxColumn khoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn luong;
        private System.Windows.Forms.DataGridViewTextBoxColumn phucap;
        private System.Windows.Forms.DataGridViewButtonColumn update;
        private System.Windows.Forms.Button button2;
    }
}