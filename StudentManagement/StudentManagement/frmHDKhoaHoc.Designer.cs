﻿namespace StudentManagement
{
    partial class frmHDKhoaHoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridMonHoc = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenMH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.combBoMon = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.combBatBuoc = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnUpdate = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridDKMH = new System.Windows.Forms.DataGridView();
            this.monhocID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ten_mh = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bomon = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.hocki = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.namhoc = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxSV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Updateeee = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMonHoc)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDKMH)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(816, 416);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.gridMonHoc);
            this.tabPage1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(808, 382);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Môn Học";
            // 
            // gridMonHoc
            // 
            this.gridMonHoc.AllowUserToDeleteRows = false;
            this.gridMonHoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridMonHoc.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridMonHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMonHoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.tenMH,
            this.combBoMon,
            this.combBatBuoc,
            this.btnUpdate});
            this.gridMonHoc.Location = new System.Drawing.Point(20, 20);
            this.gridMonHoc.Name = "gridMonHoc";
            this.gridMonHoc.Size = new System.Drawing.Size(782, 338);
            this.gridMonHoc.TabIndex = 1;
            this.gridMonHoc.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMonHoc_CellClick);
            this.gridMonHoc.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridMonHoc_RowsAdded_1);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // tenMH
            // 
            this.tenMH.HeaderText = "Môn Học";
            this.tenMH.Name = "tenMH";
            // 
            // combBoMon
            // 
            this.combBoMon.HeaderText = "Bộ Môn";
            this.combBoMon.Items.AddRange(new object[] {
            "Khoa học máy tính",
            "Hệ thống thông tin",
            "Công nghệ phần mềm",
            "Mạng máy tính"});
            this.combBoMon.Name = "combBoMon";
            // 
            // combBatBuoc
            // 
            this.combBatBuoc.HeaderText = "Bắt buộc";
            this.combBatBuoc.Items.AddRange(new object[] {
            "Có",
            "Không"});
            this.combBatBuoc.Name = "combBatBuoc";
            // 
            // btnUpdate
            // 
            this.btnUpdate.HeaderText = "Update";
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Text = "Update";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.gridDKMH);
            this.tabPage2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(808, 382);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mở môn học mới";
            // 
            // gridDKMH
            // 
            this.gridDKMH.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridDKMH.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridDKMH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDKMH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.monhocID,
            this.ten_mh,
            this.bomon,
            this.hocki,
            this.namhoc,
            this.date,
            this.maxSV,
            this.Updateeee});
            this.gridDKMH.Location = new System.Drawing.Point(10, 20);
            this.gridDKMH.Name = "gridDKMH";
            this.gridDKMH.Size = new System.Drawing.Size(792, 340);
            this.gridDKMH.TabIndex = 0;
            this.gridDKMH.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDKMH_CellClick);
            this.gridDKMH.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDKMH_CellContentClick);
            this.gridDKMH.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDKMH_RowLeave);
            this.gridDKMH.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridDKMH_RowsAdded);
            this.gridDKMH.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.gridDKMH_RowsRemoved);
            this.gridDKMH.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            this.gridDKMH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridDKMH_KeyDown);
            // 
            // monhocID
            // 
            this.monhocID.HeaderText = "monhocID";
            this.monhocID.Name = "monhocID";
            this.monhocID.Visible = false;
            // 
            // ten_mh
            // 
            this.ten_mh.HeaderText = "Môn Học";
            this.ten_mh.Name = "ten_mh";
            this.ten_mh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ten_mh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bomon
            // 
            this.bomon.HeaderText = "Bộ Môn";
            this.bomon.Items.AddRange(new object[] {
            "Khoa học máy tính",
            "Hệ thống thông tin",
            "Công nghệ phần mềm",
            "Mạng máy tính"});
            this.bomon.Name = "bomon";
            this.bomon.Visible = false;
            // 
            // hocki
            // 
            this.hocki.HeaderText = "Học Kì";
            this.hocki.Items.AddRange(new object[] {
            "1",
            "2"});
            this.hocki.Name = "hocki";
            // 
            // namhoc
            // 
            this.namhoc.HeaderText = "Năm Học";
            this.namhoc.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011"});
            this.namhoc.Name = "namhoc";
            // 
            // date
            // 
            this.date.HeaderText = "Ngày HHạn";
            this.date.Name = "date";
            this.date.ToolTipText = "15/12/2015";
            // 
            // maxSV
            // 
            this.maxSV.HeaderText = "Số SV";
            this.maxSV.Name = "maxSV";
            // 
            // Updateeee
            // 
            this.Updateeee.HeaderText = "Update";
            this.Updateeee.Name = "Updateeee";
            this.Updateeee.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Updateeee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frmHDKhoaHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 417);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmHDKhoaHoc";
            this.Text = "Hội Đồng Khoa Học";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmHDKhoaHoc_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMonHoc)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDKMH)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView gridMonHoc;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenMH;
        private System.Windows.Forms.DataGridViewComboBoxColumn combBoMon;
        private System.Windows.Forms.DataGridViewComboBoxColumn combBatBuoc;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdate;
        private System.Windows.Forms.DataGridView gridDKMH;
        private System.Windows.Forms.DataGridViewTextBoxColumn monhocID;
        private System.Windows.Forms.DataGridViewComboBoxColumn ten_mh;
        private System.Windows.Forms.DataGridViewComboBoxColumn bomon;
        private System.Windows.Forms.DataGridViewComboBoxColumn hocki;
        private System.Windows.Forms.DataGridViewComboBoxColumn namhoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxSV;
        private System.Windows.Forms.DataGridViewButtonColumn Updateeee;
    }
}