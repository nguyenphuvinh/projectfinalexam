﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmGiaoVien : Form
    {
        private String teacherId;
        public frmGiaoVien()
        {
            InitializeComponent();
            LoadData();
            comBoMonHoc.DisplayMember = "Text";
            comBoMonHoc.ValueMember = "Value";

            DatabaseConnect.Cmd.CommandText = "select ID, TEN from admin.MON_HOC where GIAO_VIEN = '"+teacherId+"'";
            DatabaseConnect.Cmd.CommandType = CommandType.Text;
            OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
            while (dr.Read())
            {
                comBoMonHoc.Items.Add(new { Text = dr.GetString(1), Value = dr.GetValue(0) });
            }
        }
        private void LoadData()
        {
            try
            {
                DatabaseConnect.Cmd.CommandText = "select p.id, p.ten, p.ngay_sinh, p.Dia_chi, k.ten, bm.ten from admin.people p, admin.khoa k, admin.bo_mon bm where bm.id = p.bo_mon and p.khoa = k.id";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                dr.Read();
                teacherId = dr.GetValue(0).ToString();
                teacherName.Text = dr.GetString(1);
                birdday.Text = dr.GetDateTime(2).ToShortDateString();
                address.Text = dr.GetString(3);
                teacherMajor.Text = dr.GetString(4);
                teacherSubject.Text = dr.GetString(5);
            }
            catch (Exception ex)
            {
                //ex.GetBaseException();
                MessageBox.Show(ex.Message);
                //MessageBox.Show("Không thể xem thông tin");
            }
        }
        private void frmGiaoVien_FormClosed(object sender, FormClosedEventArgs e)
        {
            DatabaseConnect.FrmLogin.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmGiaoVien_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void updateInfo_Click(object sender, EventArgs e)
        {
            try
            {
                DatabaseConnect.Cmd.CommandText = "update admin.people set ngay_sinh = TO_DATE('" +
                    birdday.Text + "', 'mm/dd/yyyy'), dia_chi = '" + address.Text + "'" +
                        ", TEN = '" +
                    teacherName.Text + "' " +
                        " WHERE ID = " + teacherId;
                //DatabaseConnect.Cmd.CommandType = CommandType.Text;
                DatabaseConnect.Cmd.ExecuteNonQuery();

                MessageBox.Show("Cập nhật thành công");
            }
            catch (Exception ex)
            {
                ex.GetBaseException();
                MessageBox.Show("Không thể cập nhật thông tin");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            try
            {
                gridDiem.Rows.Clear();
                DatabaseConnect.Cmd.CommandText = "select p.id,p.TEN,lop.id,lop.ten,diem.diem from admin.Diem diem " +
                "left join admin.mon_hoc mon_hoc on diem.MON_HOC = mon_hoc.ID " +
                "left join admin.lop lop on lop.ID = diem.lop " +
                "left join ADMIN.PEOPLE p on diem.SINH_VIEN = p.ID " +
                "where mon_hoc.giao_vien = " + teacherId + " and mon_hoc.ten = '" + comBoMonHoc.Text + "' and lop.hoc_ki = '" + comBoHocKi.Text + "' and lop.nam_hoc ='" + comBoNamHoc.Text + "'";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    gridDiem.Rows.Add(dr.GetValue(0), dr.GetString(1), dr.GetValue(2), dr.GetString(3), dr.GetValue(4), "Cập Nhật");
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void gridDiem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gridDiem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex == gridDiem.Columns[5].Index && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.gridDiem.Rows[e.RowIndex];
                    DatabaseConnect.Cmd.CommandText = "update admin.diem set diem = " + row.Cells[4].Value.ToString() + " where sinh_vien =" + row.Cells[0].Value.ToString() + " and lop=" + row.Cells[2].Value.ToString();
                    int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                    if (rowsUpdated != 0)
                        MessageBox.Show("Thành công!");
                    gridDiem.Rows.Clear();
                    btnXem_Click(null, null);
                }
                
            }
            catch (Exception ex)
            {
               
                MessageBox.Show("Không thể đăng kí do đã đăng kí trước đó");
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage4"])
            {
                DatabaseConnect.Cmd.CommandText = "select lop.ten,lop.mon_hoc,lop.hoc_ki,lop.nam_hoc from " +
                "admin.lop lop " +
                "left join admin.mon_hoc mon_hoc on lop.mon_hoc = mon_hoc.id " +
                "where mon_hoc.giao_vien = " + teacherId;
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                gridDSLop.Rows.Clear();
                while (dr.Read())
                {
                    gridDSLop.Rows.Add(dr.GetString(0), dr.GetValue(1), dr.GetValue(2), dr.GetValue(3), "Xem Sinh Viên");
                }
            }

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                frmThongBao tb = new frmThongBao(teacherId, "2");
                tb.Show();
            }
        }

        private void gridDSLop_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == gridDSLop.Columns[4].Index && e.RowIndex >= 0)
            {
                DataGridViewRow row = this.gridDSLop.Rows[e.RowIndex];
                frmviewSV viewsv = new frmviewSV(row.Cells[1].Value.ToString(), row.Cells[2].Value.ToString(), row.Cells[3].Value.ToString());
                viewsv.Show();
            }
        }

        private void comBoNamHoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    
}