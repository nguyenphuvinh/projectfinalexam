﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmSinhVien : Form
    {
        public frmSinhVien()
        {
            InitializeComponent();
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                DatabaseConnect.Cmd.CommandText = "select p.id, p.ten, p.ngay_sinh, p.Dia_chi, k.ten "+
                " from admin.people p, admin.khoa k where p.khoa = k.id";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                dr.Read();
                lbMSSV.Text = dr.GetValue(0).ToString();
                txtTen.Text = dr.GetString(1);
                txtNgaySinh.Text = dr.GetDateTime(2).ToString("yyyy/MM/dd");
                txtDiaChi.Text = dr.GetString(3);
                lbNganh.Text = dr.GetString(4);

                //tab Xem điểm
                comb_SV_XD_HocKi.Text = "1";
                comb_SV_XD_NamHoc.Text = "2015";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể xem thông tin");
            }
        }
        
        private void but_SV_XD_Click(object sender, EventArgs e)
        {
            try
            {
                grid_SV_XemDiem.Rows.Clear();
                DatabaseConnect.Cmd.CommandText = "select mh.ten, d.diem" +
                                    " from admin.diem d, admin.dkmonhoc dk, admin.mon_hoc mh, admin.people p " +
                                    "where d.MON_HOC = dk.MON_HOC and d.SINH_VIEN = dk.SINH_VIEN and mh.id = dk.mon_hoc and dk.SINH_VIEN = p.id " +
                                    "and dk.NAM_HOC = " + comb_SV_XD_NamHoc.Text + " and dk.HOC_KI = " + comb_SV_XD_HocKi.Text;
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();

                grid_SV_XemDiem.ColumnCount = 2;

                grid_SV_XemDiem.Columns[0].Name = "Môn học";
                grid_SV_XemDiem.Columns[1].Name = "Điểm số";
                while (dr.Read())
                {
                    grid_SV_XemDiem.Rows.Add(dr.GetString(0), dr.GetValue(1));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void but_SV_DKMH_Click(object sender, EventArgs e)
        {
            try
            {
                grid_SV_DKMH.Rows.Clear();
                grid_SV_MHDK.Rows.Clear();

                //load data cho grid đăng kí môn học
                DatabaseConnect.Cmd.CommandText = "select DISTINCT mh.ten, P.TEN, mh.id, mhn.MAX_SV, mhn.Ngay_het_han " +
                                    " from admin.MH_MO_TRONG_NAM mhn  " +
                                    " INNER JOIN admin.MON_HOC MH ON mhn.MON_HOC = MH.ID " +
                                    " INNER JOIN admin.BO_MON B ON B.ID = MH.BO_MON   " +
                                    " INNER JOIN ADMIN.PEOPLE P ON P.ID = mh.Giao_Vien  " +
                                    " where mh.giao_vien = p.id and mhn.NAM_HOC = " + comb_SV_DKMH_NamHoc.Text +
                                    " and mhn.HOC_KI = " + comb_SV_DKMH_Hocki.Text;
                                   // " and mhn.Ngay_het_han >= to_date(to_char(CURRENT_DATE,'yyyy/MM/dd'), 'yyyy/MM/dd')";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr3 = DatabaseConnect.Cmd.ExecuteReader();
                while (dr3.Read())
                {
                    grid_SV_DKMH.Rows.Add(dr3.GetString(0), dr3.GetString(1), dr3.GetValue(2), dr3.GetValue(3), dr3.GetDateTime(4).ToShortDateString(), "Đăng Kí");
                }


                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "select mh.ten, MH.ID, mhn.Ngay_het_han" +
                                    " from admin.DKMONHOC DKMH   " +
                                    " INNER JOIN admin.MH_MO_TRONG_NAM MHN ON DKMH.MON_HOC = MHN.MON_HOC  and DKMH.HOC_KI = MHN.HOC_KI AND DKMH.NAM_HOC = MHN.NAM_HOC  " +
                                    " INNER JOIN admin.MON_HOC MH ON DKMH.MON_HOC = MH.ID   " +
                                    " where DKMH.NAM_HOC = " + comb_SV_DKMH_NamHoc.Text +
                                    " and DKMH.HOC_KI = " + comb_SV_DKMH_Hocki.Text;
                                  //  " and mhn.Ngay_het_han >= to_date(to_char(CURRENT_DATE,'yyyy/MM/dd'), 'yyyy/MM/dd')";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr4 = DatabaseConnect.Cmd.ExecuteReader();
                while (dr4.Read())
                {
                    grid_SV_MHDK.Rows.Add(dr4.GetString(0), dr4.GetDateTime(2).ToShortDateString(), dr4.GetValue(1), "Hủy Đăng Kí");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

//MessageBox.Show("Không thể đăng kí do đã đăng kí trước đó");
            }

        }

        private void grid_SV_DKMH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex == grid_SV_DKMH.Columns[5].Index && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.grid_SV_DKMH.Rows[e.RowIndex];
                    DatabaseConnect.Cmd.CommandText = "Insert into admin.DKMONHOC (SINH_VIEN,MON_HOC,HOC_KI,NAM_HOC) values (" +
                    lbMSSV.Text + "," + row.Cells[2].Value.ToString() + "," + comb_SV_DKMH_Hocki.Text + "," + comb_SV_DKMH_NamHoc.Text
                        + ")";

                    int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                    if (rowsUpdated != 0)
                        MessageBox.Show("Thành công!");
                }
                grid_SV_MHDK.Rows.Clear();

                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "select mh.ten, MH.ID, mhn.Ngay_het_han" +
                                    " from admin.DKMONHOC DKMH   " +
                                    " INNER JOIN admin.MH_MO_TRONG_NAM MHN ON DKMH.MON_HOC = MHN.MON_HOC  and DKMH.HOC_KI = MHN.HOC_KI AND DKMH.NAM_HOC = MHN.NAM_HOC  " +
                                    " INNER JOIN admin.MON_HOC MH ON DKMH.MON_HOC = MH.ID   " +
                                    " where DKMH.NAM_HOC = " + comb_SV_DKMH_NamHoc.Text +
                                    " and DKMH.HOC_KI = " + comb_SV_DKMH_Hocki.Text;
                //  " and mhn.Ngay_het_han >= to_date(to_char(CURRENT_DATE,'yyyy/MM/dd'), 'yyyy/MM/dd')";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr4 = DatabaseConnect.Cmd.ExecuteReader();
                while (dr4.Read())
                {
                    grid_SV_MHDK.Rows.Add(dr4.GetString(0), dr4.GetDateTime(2).ToShortDateString(), dr4.GetValue(1), "Hủy Đăng Kí");
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                MessageBox.Show("Không thể đăng kí");
            }


        }

        private void grid_SV_MHDK_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex == grid_SV_DKMH.Columns[3].Index && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.grid_SV_MHDK.Rows[e.RowIndex];
                    DatabaseConnect.Cmd.CommandText = "DELETE admin.DKMONHOC WHERE "+
                        " MON_HOC = " + row.Cells[2].Value.ToString() + 
                        " and HOC_KI = " + comb_SV_DKMH_Hocki.Text + 
                        " and NAM_HOC = " + comb_SV_DKMH_NamHoc.Text;

                    int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                    if (rowsUpdated != 0)
                        MessageBox.Show("Thành công!");
                    else
                        MessageBox.Show("Môn học đã hết hạn hủy đăng kí!");
                }
                grid_SV_MHDK.Rows.Clear();

                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "select mh.ten, MH.ID, mhn.Ngay_het_han" +
                                    " from admin.DKMONHOC DKMH   " +
                                    " INNER JOIN admin.MH_MO_TRONG_NAM MHN ON DKMH.MON_HOC = MHN.MON_HOC  and DKMH.HOC_KI = MHN.HOC_KI AND DKMH.NAM_HOC = MHN.NAM_HOC  " +
                                    " INNER JOIN admin.MON_HOC MH ON DKMH.MON_HOC = MH.ID   " +
                                    " where DKMH.NAM_HOC = " + comb_SV_DKMH_NamHoc.Text +
                                    " and DKMH.HOC_KI = " + comb_SV_DKMH_Hocki.Text;
                //  " and mhn.Ngay_het_han >= to_date(to_char(CURRENT_DATE,'yyyy/MM/dd'), 'yyyy/MM/dd')";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr4 = DatabaseConnect.Cmd.ExecuteReader();
                while (dr4.Read())
                {
                    grid_SV_MHDK.Rows.Add(dr4.GetString(0), dr4.GetDateTime(2).ToShortDateString(), dr4.GetValue(1), "Hủy Đăng Kí");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                //MessageBox.Show("Không thể hủy đăng kí");
            }

        }

        private void frmSinhVien_FormClosed(object sender, FormClosedEventArgs e)
        {
            DatabaseConnect.FrmLogin.Show();
        }

        private void butUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DatabaseConnect.Cmd.CommandText = "UPDATE admin.PEOPLE SET " +
                         " NGAY_SINH = TO_DATE('" +
                    txtNgaySinh.Text + "', 'yyyy/mm/dd')," +
                        " TEN = '" +
                     txtTen.Text + "' " +
                         ", DIA_CHI = '" +
                    txtDiaChi.Text +
                         "' WHERE ID = " + lbMSSV.Text;
                int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                
                //DatabaseConnect.Cmd.CommandText = "UPDATE admin.PEOPLE_NAME SET " +
                //    " TEN = '" +
                // txtTen.Text + "' " +
                //    " WHERE ID = " + lbMSSV.Text;
                //rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();

                if (rowsUpdated != 0)
                    MessageBox.Show("Thành công!");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                MessageBox.Show("Không thể cập nhật");
            }
        }

        private void frmSinhVien_Load(object sender, EventArgs e)
        {

        }

        private void tabControlSinhVien_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (tabControlSinhVien.SelectedTab == tabControlSinhVien.TabPages["tabPage1"])
            {
                frmThongBao tb = new frmThongBao(lbMSSV.Text, "1");
                tb.Show();
            }
        }

    }
}
