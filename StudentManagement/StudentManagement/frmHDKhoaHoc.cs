﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmHDKhoaHoc : Form
    {
        public string bomonID;
        public string monhocDKID;
        public frmHDKhoaHoc()
        {
            InitializeComponent();
            loadData();
            loadDataForGridDHMH();
        }
        private void loadData()
        {
            try
            {
                gridMonHoc.Rows.Clear();
                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "SELECT M.ID, M.TEN, B.TEN, M.BAT_BUOC " +
                                    " FROM ADMIN.MON_HOC M " +
                                    " left JOIN ADMIN.BO_MON B ON B.ID = M.BO_MON ORDER BY m.id  ";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    gridMonHoc.Rows.Add(dr.GetValue(0),
                        dr.GetString(1),
                        dr.GetString(2),
                        (Int32.Parse(dr.GetValue(3).ToString()) == 1) ? "Có" : "Không",
                        "Update"
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridMonHoc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == gridMonHoc.Columns[4].Index && e.RowIndex >=0
                    && this.gridMonHoc.Rows[e.RowIndex].Cells[4].Value.ToString() == "Insert")
                {
                    try
                    {

                        DataGridViewRow row = this.gridMonHoc.Rows[e.RowIndex];

                        //load data cho gridview các môn đã đăng kí
                        DatabaseConnect.Cmd.CommandText = "SELECT ID " +
                                            " FROM  ADMIN.BO_MON " +
                                            " where TEN = '" + row.Cells[2].Value.ToString() + "'";
                        DatabaseConnect.Cmd.CommandType = CommandType.Text;
                        OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                        dr.Read();
                        bomonID = dr.GetValue(0).ToString();

                        DatabaseConnect.Cmd.CommandText = "Insert into ADMIN.MON_HOC (ID,TEN,BAT_BUOC,BO_MON, GIAO_VIEN) values (" +
                            row.Cells[0].Value.ToString() + ",'" +
                            row.Cells[1].Value.ToString() + "'," +
                            (row.Cells[3].Value.ToString() == "Có" ? "1" : "0") + "," +
                            bomonID +
                            ", 2 )";
                        int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();


                        if (rowsUpdated != 0)
                            MessageBox.Show("Thêm thành công!");
                        loadData();
                    }
                    catch (Exception)
                    {

                        MessageBox.Show("Không thể thêm môn học");
                    }
                }
                 else if (e.ColumnIndex == gridMonHoc.Columns[4].Index && e.RowIndex >= 0)
                 {
                     try
                     {

                         DataGridViewRow row = this.gridMonHoc.Rows[e.RowIndex];

                         //load data cho gridview các môn đã đăng kí
                         DatabaseConnect.Cmd.CommandText = "SELECT ID " +
                                             " FROM  ADMIN.BO_MON " +
                                             " where TEN = '" + row.Cells[2].Value.ToString() + "'";
                         DatabaseConnect.Cmd.CommandType = CommandType.Text;
                         OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                         dr.Read();
                         bomonID = dr.GetValue(0).ToString();

                         DatabaseConnect.Cmd.CommandText = "UPDATE admin.MON_HOC SET " +
                             " TEN = '" +
                             row.Cells[1].Value.ToString() + "'," +
                             " BO_MON = " +
                             bomonID + "," +
                             "BAT_BUOC = " +
                            (row.Cells[3].Value.ToString() == "Có" ? "1" : "0") +
                             " WHERE ID = " + row.Cells[0].Value.ToString();
                         int rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();

                         if (rowsUpdated != 0)
                             MessageBox.Show("Cập nhật thành công!");
                         loadData();
                     }
                     catch (Exception)
                     {
                         MessageBox.Show("Không thể cập nhật môn học"); ;
                     }
                 }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridMonHoc_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
        }

        private void frmHDKhoaHoc_FormClosed(object sender, FormClosedEventArgs e)
        {
            DatabaseConnect.FrmLogin.Show();
        }

        private void gridMonHoc_RowsAdded_1(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (gridMonHoc.RowCount >= 2)
            {
                gridMonHoc.Rows[gridMonHoc.RowCount - 1].Cells[4].Value = "Insert";
                gridMonHoc.Rows[gridMonHoc.RowCount - 1].Cells[0].Value = (Int64.Parse(gridMonHoc.Rows[gridMonHoc.RowCount - 2].Cells[0].Value.ToString()) + 1);
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
           // loadDataForGridDHMH();
        }

        private void loadDataForGridDHMH()
        {
            try
            {
                List<string> monhoc = new List<string>();
                ten_mh.DisplayMember = "Text";
                ten_mh.ValueMember = "Value";

                DatabaseConnect.Cmd.CommandText = "select ID, TEN from admin.MON_HOC";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!monhoc.Exists(x => x == dr.GetString(1)))
                    {
                        monhoc.Add(dr.GetString(1));
                    }
                }
                ten_mh.Items.Clear();
                foreach (string item in monhoc)
                {
                    ten_mh.Items.Add(item);
                }
                //load data cho gridview các môn đã đăng kí
                DatabaseConnect.Cmd.CommandText = "SELECT mhn.ID, M.TEN, b.ten, mhn.Hoc_ki, mhn.Nam_hoc, mhn.Ngay_het_han, mhn.Max_SV " +
                                    " FROM ADMIN.MON_HOC M " +
                                    " inner JOIN ADMIN.MH_MO_TRONG_NAM mhn ON mhn.MON_HOC = M.ID  " +
                                    " inner JOIN ADMIN.BO_MON B ON B.ID = M.BO_MON ORDER BY mhn.ID  ";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;

                OracleDataReader dr2 = DatabaseConnect.Cmd.ExecuteReader();
                while (dr2.Read())
                {
                    gridDKMH.Rows.Add(dr2.GetValue(0),
                        dr2.GetString(1),
                        dr2.GetString(2),
                        dr2.GetValue(3).ToString(),
                        dr2.GetValue(4).ToString(),
                        dr2.GetDateTime(5).ToShortDateString(),
                        dr2.GetValue(6).ToString(),
                        "Update"
                        );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridDKMH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gridDKMH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == gridDKMH.Columns[7].Index && e.RowIndex >= 0
                    && this.gridDKMH.Rows[e.RowIndex].Cells[7].Value.ToString() == "Insert")
                {
                    try
                    {

                        DataGridViewRow row = this.gridDKMH.Rows[e.RowIndex];

                        //load data cho gridview các môn đã đăng kí
                        DatabaseConnect.Cmd.CommandText = "SELECT ID " +
                                            " FROM  ADMIN.MON_HOC " +
                                            " where TEN = '" + row.Cells[1].Value.ToString() + "'";
                        DatabaseConnect.Cmd.CommandType = CommandType.Text;
                        OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                        int rowsUpdated = 0;
                        int i = 0;
                        while (dr.Read())
                        {
                            monhocDKID = dr.GetValue(0).ToString();

                            DatabaseConnect.Cmd.CommandText = "Insert into ADMIN.MH_MO_TRONG_NAM (ID, MON_HOC,HOC_KI,NAM_HOC,NGAY_HET_HAN,MAX_SV) values ( " +
                                (Int64.Parse(row.Cells[0].Value.ToString()) + i) + "," +
                                monhocDKID + "," +
                                row.Cells[3].Value.ToString() + "," +
                                row.Cells[4].Value.ToString() + ", to_date('" +
                                row.Cells[5].Value.ToString() + "', 'dd/MM/yyyy')," +
                                row.Cells[6].Value.ToString() +
                                " )";
                            rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();

                            i++;
                        }
                        if (rowsUpdated != 0)
                            MessageBox.Show("Thêm thành công!");

                        gridDKMH.Rows.Clear();
                        loadDataForGridDHMH();
                    }
                    catch (Exception)
                    {

                        MessageBox.Show("Không thể thêm môn học");
                    }
                }
                else if (e.ColumnIndex == gridDKMH.Columns[7].Index && e.RowIndex >= 0)
                {
                    try
                    {

                       DataGridViewRow row = this.gridDKMH.Rows[e.RowIndex];

                        //load data cho gridview các môn đã đăng kí
                        DatabaseConnect.Cmd.CommandText = "SELECT ID " +
                                            " FROM  ADMIN.MON_HOC " +
                                            " where TEN = '" + row.Cells[1].Value.ToString() + "'";
                        DatabaseConnect.Cmd.CommandType = CommandType.Text;
                        OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                        int rowsUpdated = 0;
                        while (dr.Read())
                        {
                            monhocDKID = dr.GetValue(0).ToString();

                            DatabaseConnect.Cmd.CommandText = "UPDATE admin.MH_MO_TRONG_NAM SET " +
                                " Mon_hoc = " +
                                monhocDKID + "," +
                                " hoc_ki = " +
                                row.Cells[3].Value.ToString() + "," +
                                " nam_hoc = " +
                                row.Cells[4].Value.ToString() + "," +
                                " max_SV = " +
                                row.Cells[6].Value.ToString() + "," +
                                " ngay_het_han = to_date('" +
                                row.Cells[5].Value.ToString() + 
                                "', 'dd/MM/yyyy') WHERE ID = " + row.Cells[0].Value.ToString();
                            rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                        }
                        if (rowsUpdated != 0)
                            MessageBox.Show("Cập nhật thành công!");

                        gridDKMH.Rows.Clear();
                        loadDataForGridDHMH();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Không thể cập nhật môn học"); ;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridDKMH_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (gridDKMH.RowCount >= 2)
            {
                gridDKMH.Rows[gridDKMH.RowCount - 1].Cells[7].Value = "Insert";
                gridDKMH.Rows[gridDKMH.RowCount - 1].Cells[0].Value = (Int64.Parse(gridDKMH.Rows[gridDKMH.RowCount - 2].Cells[0].Value.ToString()) + 1);
            }
        }

        private void gridDKMH_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                if (index > 0)
                {
                    //DataGridViewRow row = this.gridDKMH.Rows[index];
                    int rowsUpdated = 0;

                    DatabaseConnect.Cmd.CommandText = "DELETE FROM admin.MH_MO_TRONG_NAM " +
                        " WHERE ID = " + gridDKMH.Rows[index - 1].Cells[0].Value.ToString();
                    rowsUpdated = DatabaseConnect.Cmd.ExecuteNonQuery();
                    if (rowsUpdated != 0)
                        MessageBox.Show("Đã xóa thành công!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //MessageBox.Show("Không thể xóa dữ liệu"); ;
            }
        }

        private void gridDKMH_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void gridDKMH_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage1"])
            {
                gridMonHoc.Rows.Clear();
                loadData();
            }

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                gridDKMH.Rows.Clear();
                loadDataForGridDHMH();
            }
        }
    }
}
