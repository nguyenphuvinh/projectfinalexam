﻿namespace StudentManagement
{
    partial class frmThongBao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnLoad = new System.Windows.Forms.Button();
            this.gridDocThongBao = new System.Windows.Forms.DataGridView();
            this.personAnount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateAnount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnTaoTB = new System.Windows.Forms.Button();
            this.txtNoiDung = new System.Windows.Forms.RichTextBox();
            this.combKhoa = new System.Windows.Forms.ComboBox();
            this.combBoMon = new System.Windows.Forms.ComboBox();
            this.comb_ChucVu = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocThongBao)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(698, 437);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnLoad);
            this.tabPage1.Controls.Add(this.gridDocThongBao);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(690, 405);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Đọc thông báo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(297, 364);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 29);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // gridDocThongBao
            // 
            this.gridDocThongBao.AllowUserToAddRows = false;
            this.gridDocThongBao.AllowUserToDeleteRows = false;
            this.gridDocThongBao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridDocThongBao.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.gridDocThongBao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDocThongBao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.personAnount,
            this.posision,
            this.content,
            this.dateAnount});
            this.gridDocThongBao.Location = new System.Drawing.Point(30, 20);
            this.gridDocThongBao.Name = "gridDocThongBao";
            this.gridDocThongBao.ReadOnly = true;
            this.gridDocThongBao.Size = new System.Drawing.Size(639, 325);
            this.gridDocThongBao.TabIndex = 1;
            // 
            // personAnount
            // 
            this.personAnount.HeaderText = "Người Thông Báo";
            this.personAnount.Name = "personAnount";
            this.personAnount.ReadOnly = true;
            // 
            // posision
            // 
            this.posision.HeaderText = "Chức Vụ Người Thông Báo";
            this.posision.Name = "posision";
            this.posision.ReadOnly = true;
            // 
            // content
            // 
            this.content.HeaderText = "Nội Dung";
            this.content.Name = "content";
            this.content.ReadOnly = true;
            // 
            // dateAnount
            // 
            this.dateAnount.HeaderText = "Ngày Thông Báo";
            this.dateAnount.Name = "dateAnount";
            this.dateAnount.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnTaoTB);
            this.tabPage2.Controls.Add(this.txtNoiDung);
            this.tabPage2.Controls.Add(this.combKhoa);
            this.tabPage2.Controls.Add(this.combBoMon);
            this.tabPage2.Controls.Add(this.comb_ChucVu);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(690, 405);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tạo thông báo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnTaoTB
            // 
            this.btnTaoTB.Location = new System.Drawing.Point(284, 328);
            this.btnTaoTB.Name = "btnTaoTB";
            this.btnTaoTB.Size = new System.Drawing.Size(128, 37);
            this.btnTaoTB.TabIndex = 3;
            this.btnTaoTB.Text = "Tạo thông báo";
            this.btnTaoTB.UseVisualStyleBackColor = true;
            this.btnTaoTB.Click += new System.EventHandler(this.btnTaoTB_Click);
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.Location = new System.Drawing.Point(111, 185);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Size = new System.Drawing.Size(526, 117);
            this.txtNoiDung.TabIndex = 2;
            this.txtNoiDung.Text = "";
            // 
            // combKhoa
            // 
            this.combKhoa.FormattingEnabled = true;
            this.combKhoa.Items.AddRange(new object[] {
            "Công nghệ thông tin",
            "Vật lý",
            "Hóa học"});
            this.combKhoa.Location = new System.Drawing.Point(111, 133);
            this.combKhoa.Name = "combKhoa";
            this.combKhoa.Size = new System.Drawing.Size(211, 27);
            this.combKhoa.TabIndex = 1;
            // 
            // combBoMon
            // 
            this.combBoMon.FormattingEnabled = true;
            this.combBoMon.Items.AddRange(new object[] {
            "Hệ thống thông tin",
            "Khoa học máy tính"});
            this.combBoMon.Location = new System.Drawing.Point(111, 80);
            this.combBoMon.Name = "combBoMon";
            this.combBoMon.Size = new System.Drawing.Size(211, 27);
            this.combBoMon.TabIndex = 1;
            // 
            // comb_ChucVu
            // 
            this.comb_ChucVu.FormattingEnabled = true;
            this.comb_ChucVu.Items.AddRange(new object[] {
            "Sinh Viên",
            "Giáo Viên",
            "Trưởng Khoa"});
            this.comb_ChucVu.Location = new System.Drawing.Point(111, 32);
            this.comb_ChucVu.Name = "comb_ChucVu";
            this.comb_ChucVu.Size = new System.Drawing.Size(211, 27);
            this.comb_ChucVu.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nội Dung";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Khoa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bộ Môn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người đọc";
            // 
            // frmThongBao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 435);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmThongBao";
            this.Text = "Thông Báo";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDocThongBao)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.DataGridView gridDocThongBao;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnTaoTB;
        private System.Windows.Forms.RichTextBox txtNoiDung;
        private System.Windows.Forms.ComboBox combKhoa;
        private System.Windows.Forms.ComboBox combBoMon;
        private System.Windows.Forms.ComboBox comb_ChucVu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn personAnount;
        private System.Windows.Forms.DataGridViewTextBoxColumn posision;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateAnount;
    }
}