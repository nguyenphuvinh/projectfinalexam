﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmviewSV : Form
    {
        
        public frmviewSV(String monhoc, String hocki, String namhoc)
        {
            InitializeComponent();
            DatabaseConnect.Cmd.CommandText = "select p.id,p.TEN from " +
            "admin.dkmonhoc dkmonhoc " +
            "left join ADMIN.PEOPLE p on p.id = dkmonhoc.sinh_vien " +
            "where dkmonhoc.mon_hoc = '" + monhoc + "' and dkmonhoc.hoc_ki = '" + hocki + "' and dkmonhoc.nam_hoc ='" + namhoc + "'";
            DatabaseConnect.Cmd.CommandType = CommandType.Text;

            OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
            while (dr.Read())
            {
                gridSV.Rows.Add(dr.GetValue(0), dr.GetString(1));
            }
        }
    }
}
