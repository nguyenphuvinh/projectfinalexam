﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmLogin : Form
    {
        string peopleID;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void butLogin_Click(object sender, EventArgs e)
        {
            try
            {
                DatabaseConnect.Oradb = "User Id=" + txtUsername.Text.ToUpper() + ";Password=" + txtPassword.Text + ";Data Source=" +
                                 "(DESCRIPTION =" +
                                "(ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))" +
                                "(CONNECT_DATA =" +
                                  "(SERVER = DEDICATED)" +
                                  "(SERVICE_NAME = University)" +
                                ")" +
                              ")";
                DatabaseConnect.Conn = new OracleConnection(DatabaseConnect.Oradb);  // C#
                DatabaseConnect.Conn.Open();
                DatabaseConnect.Cmd.Connection = DatabaseConnect.Conn;
                //cmd.CommandText = "select p.ten, c.ten from admin.people p, admin.chuc_vu c where p.chuc_vu = c.id and username = '" + txtUsername.Text + "'";
                DatabaseConnect.Cmd.CommandText = "select p.ten, c.ten, p.id,c.id from admin.people p, admin.chuc_vu c where p.chuc_vu = c.id AND P.USERNAME = '" + txtUsername.Text.ToUpper()+"'";
                DatabaseConnect.Cmd.CommandType = CommandType.Text;
                OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
                dr.Read();
                peopleID = dr.GetValue(2).ToString();
                MessageBox.Show("Chào mừng " + dr.GetString(1) + " " + dr.GetString(0));
                DatabaseConnect.FrmLogin =  this;
                DatabaseConnect.FrmLogin.Hide();
                OpenForm(
                    Int32.Parse(dr.GetValue(3).ToString())
                    );
                //DatabaseConnect.Conn.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể đăng nhập. Hãy nhập lại Username và Password");
            }
        }
        private void OpenForm(int formName)
        {
            switch (formName) {
                case 1:
                    frmSinhVien SinhVien = new frmSinhVien();
                    SinhVien.Show();
                    break;
                case 2:
                    frmGiaoVien GiaoVien = new frmGiaoVien();
                    GiaoVien.Show();
                    break;
                case 3:
                    break;
                case 4:
                    frmHDKhoaHoc HDKH = new frmHDKhoaHoc();
                    HDKH.Show();
                    break;
                case 5:
                    break;
                case 6:
                    frmQLNhanSu QLNhanSu = new frmQLNhanSu();
                    QLNhanSu.Show();
                    break;
                case 7:
                    frmThongBao truongkhoa = new frmThongBao(peopleID.ToString(), formName.ToString());
                    truongkhoa.Show();
                    break;
                default:
                    break;
            }
        }
    }
}
