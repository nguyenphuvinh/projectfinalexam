﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.DataAccess.Types;
using StudentManagement.Class;

namespace StudentManagement
{
    public partial class frmThemNhanVien : Form
    {
        public frmThemNhanVien()
        {
            InitializeComponent();
            txtNgaySinh.CustomFormat = "yyyy/MM/dd";
            comb_ChucVu.Items.Clear();
            comb_BoMon.Items.Clear();
            comb_Khoa.Items.Clear();

            comb_ChucVu.DisplayMember = "Text";
            comb_ChucVu.ValueMember = "Value";

            DatabaseConnect.Cmd.CommandText = "select ID, TEN from admin.Chuc_Vu where id != 1 ";
            DatabaseConnect.Cmd.CommandType = CommandType.Text;
            OracleDataReader dr = DatabaseConnect.Cmd.ExecuteReader();
            while (dr.Read())
            {
                comb_ChucVu.Items.Add(new { Text = dr.GetString(1), Value = dr.GetValue(0) });
            }

            comb_BoMon.DisplayMember = "Text";
            comb_BoMon.ValueMember = "Value";

            DatabaseConnect.Cmd.CommandText = "select ID, TEN from admin.Bo_Mon ";
            DatabaseConnect.Cmd.CommandType = CommandType.Text;
            OracleDataReader dr1 = DatabaseConnect.Cmd.ExecuteReader();
            while (dr1.Read())
            {
                comb_BoMon.Items.Add(new { Text = dr1.GetString(1), Value = dr1.GetValue(0) });
            }

            comb_Khoa.DisplayMember = "Text";
            comb_Khoa.ValueMember = "Value";

            DatabaseConnect.Cmd.CommandText = "select ID, TEN from admin.Khoa ";
            DatabaseConnect.Cmd.CommandType = CommandType.Text;
            OracleDataReader dr2 = DatabaseConnect.Cmd.ExecuteReader();
            while (dr2.Read())
            {
                comb_Khoa.Items.Add(new { Text = dr2.GetString(1), Value = dr2.GetValue(0) });
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DatabaseConnect.Cmd.CommandText = "Insert into admin.people (ID, TEN, CHUC_VU,BO_MON,USERNAME,KHOA,NGAY_SINH,DIA_CHI) values (" +
                   txtID.Text + ", '" +
                   txtTen.Text + "', " +
                    (comb_ChucVu.SelectedIndex + 2).ToString() + ", " +
                    (comb_BoMon.SelectedIndex + 1).ToString() + ", '" +
                    txtUsername.Text + "', " +
                    (comb_Khoa.SelectedIndex + 1).ToString() + ", TO_DATE('" +
                    txtNgaySinh.Value.ToString("yyyy/MM/dd") + "', 'yyyy/mm/dd'), '" +
                    txtDiaChi.Text
                        + "')";
                int rowsInserted = DatabaseConnect.Cmd.ExecuteNonQuery();

                DatabaseConnect.Cmd.CommandText = "Insert into admin.Luong (NHAN_VIEN, LUONG, PHU_CAP) values (" +
                   txtID.Text + ", " +
                    txtLuong.Text + ", " +
                   txtPhuCap.Text
                       + ")";
                rowsInserted = DatabaseConnect.Cmd.ExecuteNonQuery();

                if (rowsInserted != 0)
                    MessageBox.Show("Thành công!");
                

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                MessageBox.Show("Không thể cập nhật do lỗi hệ thống.");
            }
        }

        private void frmThemNhanVien_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
