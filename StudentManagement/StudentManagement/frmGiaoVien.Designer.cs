﻿namespace StudentManagement
{
    partial class frmGiaoVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.address = new System.Windows.Forms.TextBox();
            this.birdday = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.updateInfo = new System.Windows.Forms.Button();
            this.teacherSubject = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.teacherMajor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.teacherName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnXem = new System.Windows.Forms.Button();
            this.comBoMonHoc = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comBoNamHoc = new System.Windows.Forms.ComboBox();
            this.comBoHocKi = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gridDiem = new System.Windows.Forms.DataGridView();
            this.idSinhVien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idLop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.update = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gridDSLop = new System.Windows.Forms.DataGridView();
            this.tenlop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hocki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namhoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewsv = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDiem)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDSLop)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(-1, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(599, 309);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.address);
            this.tabPage1.Controls.Add(this.birdday);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.updateInfo);
            this.tabPage1.Controls.Add(this.teacherSubject);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.teacherMajor);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.teacherName);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(591, 277);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông Tin";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // address
            // 
            this.address.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address.Location = new System.Drawing.Point(403, 74);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(175, 26);
            this.address.TabIndex = 10;
            // 
            // birdday
            // 
            this.birdday.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birdday.Location = new System.Drawing.Point(402, 34);
            this.birdday.Name = "birdday";
            this.birdday.Size = new System.Drawing.Size(178, 26);
            this.birdday.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(330, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 19);
            this.label5.TabIndex = 8;
            this.label5.Text = "Địa Chỉ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(329, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ngày Sinh";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // updateInfo
            // 
            this.updateInfo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateInfo.Location = new System.Drawing.Point(12, 171);
            this.updateInfo.Name = "updateInfo";
            this.updateInfo.Size = new System.Drawing.Size(81, 30);
            this.updateInfo.TabIndex = 6;
            this.updateInfo.Text = "Cập Nhật";
            this.updateInfo.UseVisualStyleBackColor = true;
            this.updateInfo.Click += new System.EventHandler(this.updateInfo_Click);
            // 
            // teacherSubject
            // 
            this.teacherSubject.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherSubject.Location = new System.Drawing.Point(97, 112);
            this.teacherSubject.Name = "teacherSubject";
            this.teacherSubject.ReadOnly = true;
            this.teacherSubject.Size = new System.Drawing.Size(204, 26);
            this.teacherSubject.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Bộ Môn";
            // 
            // teacherMajor
            // 
            this.teacherMajor.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherMajor.Location = new System.Drawing.Point(97, 75);
            this.teacherMajor.Name = "teacherMajor";
            this.teacherMajor.ReadOnly = true;
            this.teacherMajor.Size = new System.Drawing.Size(204, 26);
            this.teacherMajor.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Khoa";
            // 
            // teacherName
            // 
            this.teacherName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherName.Location = new System.Drawing.Point(97, 35);
            this.teacherName.Name = "teacherName";
            this.teacherName.Size = new System.Drawing.Size(204, 26);
            this.teacherName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnXem);
            this.tabPage3.Controls.Add(this.comBoMonHoc);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.comBoNamHoc);
            this.tabPage3.Controls.Add(this.comBoHocKi);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.gridDiem);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(591, 277);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Cập Nhật Điểm";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnXem
            // 
            this.btnXem.Location = new System.Drawing.Point(486, 15);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(75, 29);
            this.btnXem.TabIndex = 3;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // comBoMonHoc
            // 
            this.comBoMonHoc.FormattingEnabled = true;
            this.comBoMonHoc.Location = new System.Drawing.Point(352, 17);
            this.comBoMonHoc.Name = "comBoMonHoc";
            this.comBoMonHoc.Size = new System.Drawing.Size(121, 27);
            this.comBoMonHoc.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(278, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Môn Học";
            // 
            // comBoNamHoc
            // 
            this.comBoNamHoc.FormattingEnabled = true;
            this.comBoNamHoc.Items.AddRange(new object[] {
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010"});
            this.comBoNamHoc.Location = new System.Drawing.Point(141, 17);
            this.comBoNamHoc.Name = "comBoNamHoc";
            this.comBoNamHoc.Size = new System.Drawing.Size(121, 27);
            this.comBoNamHoc.TabIndex = 2;
            this.comBoNamHoc.SelectedIndexChanged += new System.EventHandler(this.comBoNamHoc_SelectedIndexChanged);
            // 
            // comBoHocKi
            // 
            this.comBoHocKi.FormattingEnabled = true;
            this.comBoHocKi.Items.AddRange(new object[] {
            "1",
            "2"});
            this.comBoHocKi.Location = new System.Drawing.Point(84, 17);
            this.comBoHocKi.Name = "comBoHocKi";
            this.comBoHocKi.Size = new System.Drawing.Size(51, 27);
            this.comBoHocKi.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "Học Kì";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // gridDiem
            // 
            this.gridDiem.AllowUserToAddRows = false;
            this.gridDiem.AllowUserToDeleteRows = false;
            this.gridDiem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridDiem.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridDiem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDiem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idSinhVien,
            this.name,
            this.idLop,
            this.lop,
            this.diem,
            this.update});
            this.gridDiem.Location = new System.Drawing.Point(31, 68);
            this.gridDiem.Name = "gridDiem";
            this.gridDiem.Size = new System.Drawing.Size(530, 186);
            this.gridDiem.TabIndex = 0;
            this.gridDiem.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDiem_CellClick);
            this.gridDiem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDiem_CellContentClick);
            // 
            // idSinhVien
            // 
            this.idSinhVien.HeaderText = "idsv";
            this.idSinhVien.Name = "idSinhVien";
            this.idSinhVien.Visible = false;
            // 
            // name
            // 
            this.name.HeaderText = "Tên Sinh Viên";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // idLop
            // 
            this.idLop.HeaderText = "idLop";
            this.idLop.Name = "idLop";
            this.idLop.Visible = false;
            // 
            // lop
            // 
            this.lop.HeaderText = "Tên Lớp";
            this.lop.Name = "lop";
            this.lop.ReadOnly = true;
            // 
            // diem
            // 
            this.diem.HeaderText = "Điểm";
            this.diem.Name = "diem";
            // 
            // update
            // 
            this.update.HeaderText = "Cập Nhật";
            this.update.Name = "update";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gridDSLop);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(591, 277);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Danh Sách Lớp";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gridDSLop
            // 
            this.gridDSLop.AllowUserToAddRows = false;
            this.gridDSLop.AllowUserToDeleteRows = false;
            this.gridDSLop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridDSLop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDSLop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenlop,
            this.id,
            this.hocki,
            this.namhoc,
            this.viewsv});
            this.gridDSLop.Location = new System.Drawing.Point(28, 41);
            this.gridDSLop.Name = "gridDSLop";
            this.gridDSLop.ReadOnly = true;
            this.gridDSLop.Size = new System.Drawing.Size(534, 211);
            this.gridDSLop.TabIndex = 0;
            this.gridDSLop.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDSLop_CellClick);
            // 
            // tenlop
            // 
            this.tenlop.HeaderText = "Tên Lớp";
            this.tenlop.Name = "tenlop";
            this.tenlop.ReadOnly = true;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // hocki
            // 
            this.hocki.HeaderText = "Học Kì";
            this.hocki.Name = "hocki";
            this.hocki.ReadOnly = true;
            // 
            // namhoc
            // 
            this.namhoc.HeaderText = "Năm Học";
            this.namhoc.Name = "namhoc";
            this.namhoc.ReadOnly = true;
            // 
            // viewsv
            // 
            this.viewsv.HeaderText = "Xem Sinh Viên";
            this.viewsv.Name = "viewsv";
            this.viewsv.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(591, 277);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Thông Báo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // frmGiaoVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 316);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmGiaoVien";
            this.Text = "Giáo Viên";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmGiaoVien_FormClosed);
            this.Load += new System.EventHandler(this.frmGiaoVien_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDiem)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDSLop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox teacherSubject;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox teacherMajor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox teacherName;
        private System.Windows.Forms.DataGridView gridDiem;
        private System.Windows.Forms.DataGridView gridDSLop;
        private System.Windows.Forms.Button updateInfo;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.TextBox birdday;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comBoMonHoc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comBoNamHoc;
        private System.Windows.Forms.ComboBox comBoHocKi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnXem;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSinhVien;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn idLop;
        private System.Windows.Forms.DataGridViewTextBoxColumn lop;
        private System.Windows.Forms.DataGridViewTextBoxColumn diem;
        private System.Windows.Forms.DataGridViewButtonColumn update;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenlop;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn hocki;
        private System.Windows.Forms.DataGridViewTextBoxColumn namhoc;
        private System.Windows.Forms.DataGridViewButtonColumn viewsv;
        private System.Windows.Forms.TabPage tabPage2;
    }
}