--ALTER WALLET
ALTER SYSTEM SET ENCRYPTION WALLET OPEN IDENTIFIED BY "abc12345";
ALTER SYSTEM SET ENCRYPTION WALLET CLOSE IDENTIFIED BY "abc12345";

-- VPD: check user login vao he thong

create or replace FUNCTION "SEC_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
Begin
if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
return '' ;
elsif ( SYS_CONTEXT('people_CV', 'chucvu') = '7' ) then
return '' ;
elsif ( SYS_CONTEXT('people_CV', 'chucvu') = '6' ) then
return 'CHUC_VU != 1' ;
elsif ( SYS_CONTEXT('people_CV', 'chucvu') = '5' ) then
return 'CHUC_VU = 1 OR USERNAME = SYS_CONTEXT(''USERENV'', ''SESSION_USER'')' ;
else
return 'USERNAME = SYS_CONTEXT(''USERENV'', ''SESSION_USER'') ' ;
end if;
End;

create or replace FUNCTION "EDIT_SEC_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
Begin
if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
return '' ;
elsif ( SYS_CONTEXT('people_CV', 'chucvu') = '6' ) then
return 'CHUC_VU != 1' ;
elsif ( SYS_CONTEXT('people_CV', 'chucvu') = '5' ) then
return 'CHUC_VU = 1 OR USERNAME = SYS_CONTEXT(''USERENV'', ''SESSION_USER'')' ;
else
return 'USERNAME = SYS_CONTEXT(''USERENV'', ''SESSION_USER'') ' ;
end if;
End;

-- POLICY SELECT ON PEOPLE
begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'PEOPLE',
policy_name => 'PEOPLE_POLICY',
function_schema => 'ADMIN',
policy_function => 'sec_function',
statement_types => 'select',
sec_relevant_cols => 'CHUC_VU, BO_MON, NGAY_SINH, DIA_CHI, USERNAME, KHOA',
sec_relevant_cols_opt => DBMS_RLS.all_rows, -- Chi co tac dung voi SELECT, cho phep xem tat ca cac cot nhung gia tri la null
update_check => TRUE);
end;
--- POLICY EDIT ON PEOPLE
begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'PEOPLE',
policy_name => 'EDIT_PEOPLE_POLICY',
function_schema => 'ADMIN',
policy_function => 'EDIT_SEC_FUNCTION',
statement_types => 'insert, update',
sec_relevant_cols => 'ID, TEN, CHUC_VU, BO_MON, NGAY_SINH, DIA_CHI, USERNAME, KHOA',
update_check => TRUE);
end;
--- DELETE Policy ---
/*BEGIN
DBMS_RLS.drop_policy
(object_schema => 'ADMIN',
object_name => 'PEOPLE',
policy_name => 'EDIT_PEOPLE_POLICY');
END;*/

-- VPD XEM DIEM
create or replace FUNCTION "DIEM_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
  user_id number;
  chuc_vu number;
  monhoc varchar2(200) := '';
Begin
  if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
    return '' ;
  end if;
    
  select id into user_id from admin.People where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.chuc_vu into chuc_vu from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  
  if(chuc_vu = 1) then  
    return 'SINH_VIEN = '||user_id ;
  elsif(chuc_vu != 1 and chuc_vu != 7 and chuc_vu != 5) then
    return 'MON_HOC IN ( select ID from admin.mon_hoc M where M.GIAO_VIEN = '||user_id||')' ;
  elsif(chuc_vu = 7 or chuc_vu = 5) then
    return '' ;
  end if;
End;

begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'DIEM',
policy_name => 'DIEM_POLICY',
function_schema => 'ADMIN',
policy_function => 'DIEM_FUNCTION',
statement_types => 'select, update, insert',
update_check => TRUE
);
end;
-- DELETE DIEM
/*BEGIN
DBMS_RLS.drop_policy
(object_schema => 'ADMIN',
object_name => 'DIEM',
policy_name => 'DIEM_POLICY');
END;*/

---VPD DANG KI MON HOC
create or replace FUNCTION "DKMH_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
  user_id number;
  chuc_vu number;
Begin
  if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
    return '' ;
  end if;
    
  select id into user_id from admin.People where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.chuc_vu into chuc_vu from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  
  if(chuc_vu = 1) then  
    return 'SINH_VIEN = '|| user_id;
  elsif(chuc_vu = 2) then
    return 'MON_HOC IN ( select ID from admin.mon_hoc M where M.GIAO_VIEN = '||user_id||')' ;
  elsif(chuc_vu = 7 or chuc_vu = 5) then
    return '' ;
  end if;
End;

begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'DKMONHOC',
policy_name => 'DKMH_POLICY',
function_schema => 'ADMIN',
policy_function => 'DKMH_FUNCTION',
statement_types => 'select'
);
end;

-- VPD KHI EDIT DANG KI MON HOC
create or replace FUNCTION "EDIT_DKMH_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
  user_id number;
  chuc_vu number;
Begin
  if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
    return '' ;
  end if;
    
  select id into user_id from admin.People where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.chuc_vu into chuc_vu from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  
  if(chuc_vu = 1) then  
    return 'SINH_VIEN = '||user_id || ' and MON_HOC in (select mhn.Mon_hoc 
    from admin.MH_MO_TRONG_NAM mhn
    where mhn.NGAY_HET_HAN >= to_date(to_char(CURRENT_DATE,''yyyy/MM/dd''), ''yyyy/MM/dd''))'  ;
  end if;
End;

begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'DKMONHOC',
policy_name => 'EDIT_DKMH_POLICY',
function_schema => 'ADMIN',
policy_function => 'EDIT_DKMH_FUNCTION',
statement_types => 'update, insert, delete',
update_check => TRUE 
);
end;

-- VPD LUONG
create or replace FUNCTION "LUONG_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
  user_id number;
  chuc_vu number;
Begin
  if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
    return '' ;
  end if;
    
  select id into user_id from admin.People where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.chuc_vu into chuc_vu from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  
  if(chuc_vu != 6) then  
    return 'NHAN_VIEN = '||user_id ;
  elsif(chuc_vu = 6) then
    return '' ;
  end if;
End;

begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'LUONG',
policy_name => 'LUONG_POLICY',
function_schema => 'ADMIN',
policy_function => 'LUONG_FUNCTION',
statement_types => 'select, update, insert',
update_check => TRUE
);
end;

--- VPD LICH DAY
create or replace FUNCTION "LICHDAY_FUNCTION" (p_schema varchar2, p_obj varchar2)
Return varchar2
As
  user_id number;
  chuc_vu number;
  bo_mon number;
Begin
  if ( SYS_CONTEXT('userenv', 'SESSION_USER') = 'ADMIN' ) then
    return '' ;
  end if;
    
  select id into user_id from admin.People where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.chuc_vu into chuc_vu from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  select p.bo_mon into bo_mon from admin.People p where username = SYS_CONTEXT('userenv', 'SESSION_USER');
  
  if(chuc_vu != 3 and chuc_vu != 5 and chuc_vu != 7) then  
    return 'GIAO_VIEN = '||user_id ;
  elsif(chuc_vu = 7 or chuc_vu = 5) then
    return '' ;
  elsif(chuc_vu = 3) then
    return 'GIAO_VIEN IN (SELECT GIAO_VIEN FROM ADMIN.MON_HOC M WHERE M.BO_MON = '|| bo_mon||')';
  end if;
End;

begin dbms_rls.add_policy (object_schema => 'ADMIN',
object_name => 'LICH_DAY',
policy_name => 'LICHDAY_POLICY',
function_schema => 'ADMIN',
policy_function => 'LICHDAY_FUNCTION',
statement_types => 'select, update, insert',
update_check => TRUE
);
end;

--- DELETE Policy ---
/*BEGIN
DBMS_RLS.drop_policy
(object_schema => 'ADMIN',
object_name => 'DKMONHOC',
policy_name => 'DKMH_POLICY');
END;*/

-- Tạo role SINHVIEN và cấp quyen
create role SINHVIEN;
GRANT CREATE USER TO SINHVIEN;
grant create session to SINHVIEN;
grant select, update on PEOPLE to SINHVIEN;
grant select on CHUC_VU to SINHVIEN;
grant select on KHOA to SINHVIEN;
grant select on DIEM to SINHVIEN;
grant select, insert, delete on DKMONHOC to SINHVIEN;
grant select on MON_HOC to SINHVIEN;
grant select on MH_MO_TRONG_NAM to SINHVIEN;
grant select on bo_mon to SINHVIEN;
grant select, insert on thong_bao to SINHVIEN;

-- Tạo user
create user NVA IDENTIFIED by "123456";
create user NTL IDENTIFIED by "123456";

-- Thêm user vào ROLE SINHVIEN
GRANT SINHVIEN TO NVA;
GRANT SINHVIEN TO NTL;

--
-- Tạo role giaovien và cấp quyen
create role giaovien;
GRANT CREATE USER TO giaovien;
grant create session to giaovien;
grant select on lich_day to giaovien;
grant select on luong to giaovien;
grant select on bo_mon to giaovien;
grant select,update,insert on diem to giaovien;
grant select, update on PEOPLE to giaovien;
grant select on CHUC_VU to giaovien;
grant select on KHOA to giaovien;
grant select on DKMONHOC to giaovien;
grant select on MON_HOC to giaovien;
grant select on MH_MO_TRONG_NAM to giaovien;
grant select on LOP to giaovien;
grant select,insert on thong_bao to giaovien;

-- Tạo user
create user NTB IDENTIFIED by "123456";
GRANT giaovien TO NTB;


-- ROLE QUAN LY NHAN SU
create role QLNHANSU;
GRANT CREATE USER TO QLNHANSU;
grant create session to QLNHANSU;
grant select, INSERT, UPDATE on PEOPLE to QLNHANSU;
grant select, INSERT, UPDATE on LUONG to QLNHANSU;
grant select on BO_MON to QLNHANSU;
grant select on KHOA to QLNHANSU;
grant select on CHUC_VU to QLNHANSU;

create user TVT IDENTIFIED by "123456";
GRANT QLNHANSU TO TVT;

-- ROLE TRUONG PHO KHOA
create role TRUONGPHOKHOA;
GRANT CREATE USER TO TRUONGPHOKHOA;
grant create session to TRUONGPHOKHOA;
grant select on lich_day to TRUONGPHOKHOA;
grant select on luong to TRUONGPHOKHOA;
grant select on bo_mon to TRUONGPHOKHOA;
grant select on diem to TRUONGPHOKHOA;
grant select, update on PEOPLE to TRUONGPHOKHOA;
grant select on CHUC_VU to TRUONGPHOKHOA;
grant select on KHOA to TRUONGPHOKHOA;
grant select on DKMONHOC to TRUONGPHOKHOA;
grant select on MON_HOC to TRUONGPHOKHOA;
grant select on MH_MO_TRONG_NAM to TRUONGPHOKHOA;
grant select on LOP to TRUONGPHOKHOA;
grant select, insert on thong_bao to TRUONGPHOKHOA;

create user LVA IDENTIFIED by "123456";
GRANT TRUONGPHOKHOA TO LVA;


-- ROLE HOI DONG KHOA HOC
create role HDKHOAHOC;
GRANT CREATE USER TO HDKHOAHOC;
grant create session to HDKHOAHOC;
grant select, update on PEOPLE to HDKHOAHOC;
grant select on CHUC_VU to HDKHOAHOC;
grant select, insert, update on MON_HOC to HDKHOAHOC;
grant select on KHOA to HDKHOAHOC;
grant select on BO_MON to HDKHOAHOC;
grant select, insert, update, DELETE on MH_MO_TRONG_NAM to HDKHOAHOC;

create user HVD IDENTIFIED by "123456";
GRANT HDKHOAHOC TO HVD;

-- ROLE TRUONG BO MON
create role TRUONGBOMON;
GRANT CREATE USER TO TRUONGBOMON;
grant create session to TRUONGBOMON;
grant select, insert, update, delete on lich_day to TRUONGBOMON;
grant select on luong to TRUONGBOMON;
grant select on bo_mon to TRUONGBOMON;
grant select, update on PEOPLE to TRUONGBOMON;
grant select on CHUC_VU to TRUONGBOMON;
grant select on KHOA to TRUONGBOMON;
grant select on MON_HOC to TRUONGBOMON;
grant select on MH_MO_TRONG_NAM to TRUONGBOMON;
grant select, insert, update on LOP to TRUONGBOMON;

create user TVC IDENTIFIED by "123456";
GRANT TRUONGBOMON TO TVC;

--- ROLE GIAO VU

create role GIAOVU;
GRANT CREATE USER TO GIAOVU;
grant create session to GIAOVU;
grant select, update on lich_day to GIAOVU;
grant select on DKMONHOC to GIAOVU;
grant select on MON_HOC to GIAOVU;
grant select, INSERT, UPDATE on PEOPLE to GIAOVU;
grant select on diem to GIAOVU;

create user HTP IDENTIFIED by "123456";
GRANT GIAOVU TO HTP;