---I. AUDIT

/*
	B1: De kich hoat chuc nang giam sat, lam theo cac buoc sau
	Dang nhap bang tai khoan SYS voi quyen SYSDBA
	audit_sys_operations nen dat la TRUE neu muon ghi lai hoat dong cua cac user SYSDBA
	audit_trail nen dat that DB_EXTENDED neu muon luu lai cau SQL user da thuc thi
*/
  ALTER SYSTEM SET AUDIT_SYS_OPERATIONS=TRUE SCOPE=SPFILE;
  ALTER SYSTEM SET AUDIT_TRAIL=DB_EXTENDED SCOPE=SPFILE;
  
-- B2: Khoi dong lai database
  SHUTDOWN IMMEDIATE;
  STARTUP;		
 
-- B3: Tien hanh Audit User va Table
-- Audit Select, Update cua user Quan Ly Nhan Su “TVT”
  AUDIT SELECT TABLE, UPDATE TABLE BY TVT;
-- Audit thong tin tren table Luong và table People
  AUDIT ALL ON admin.LUONG BY ACCESS;
  AUDIT ALL ON admin.PEOPLE BY ACCESS;
--B4: Xem thong tin Audit:
  SELECT username,obj_name,action_name, timestamp, sql_text 
  FROM   dba_audit_trail
  WHERE  username = 'TVT'
  ORDER BY timestamp DESC; 

--- II. MA HOA 

--B1: Mo ket noi den Wallet
  ALTER SYSTEM SET ENCRYPTION WALLET OPEN IDENTIFIED BY "abc12345";
  
--B2: De ma hoa cot Luong trong table Luong, ta lam nhu sau
  ALTER TABLE ADMIN.LUONG MODIFY (LUONG ENCRYPT);

--B3:	Thay doi phuong phap ma hoa tu AES192 bit sang AES128:
  ALTER TABLE ADMIN.LUONG MODIFY (LUONG ENCRYPT USING 'AES128');
--Co the dung AES128, AES192, AES256, hay 3DES168 (thuat toan DES 168-bit Triple).

--B4: De kiem tra xem cot LUONG da ma hoa chua, su dung cau lenh:
  SELECT * FROM DBA_ENCRYPTED_COLUMNS 
  where table_name = 'LUONG';

