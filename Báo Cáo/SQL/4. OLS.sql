-- tao user quan li
GRANT connect, create user, drop user,create role, drop any role TO ad_sec IDENTIFIED BY adsec;
GRANT connect TO sec_admin IDENTIFIED BY secadmin;
--tao policy
CONN lbacsys/lbacsys;
BEGIN 
  SA_SYSDBA.CREATE_POLICY (
  policy_name => 'XEM_THONG_BAO',
  column_name => 'OLS_NUM');
END; 
-- them quyen cho 2 user nay
CONN lbacsys/lbacsys;
GRANT XEM_THONG_BAO_dba TO sec_admin;
GRANT execute ON sa_components TO sec_admin;
GRANT execute ON sa_label_admin TO sec_admin;
GRANT execute ON sa_policy_admin TO sec_admin; 
CONN lbacsys/lbacsys;
GRANT XEM_THONG_BAO_dba TO ad_sec;
GRANT execute ON sa_user_admin TO ad_sec; 

-- tao level cho ols
CONN sec_admin/secadmin;
EXECUTE sa_components.create_level('XEM_THONG_BAO',1000,'sv','sinhvien');
EXECUTE sa_components.create_level('XEM_THONG_BAO',2000,'gv','giaovien');
EXECUTE sa_components.create_level('XEM_THONG_BAO',3000,'tk','truongkhoa');
EXECUTE sa_components.create_level('XEM_THONG_BAO',4000,'tbm','truongbomon');
-- tao comparment
CONN sec_admin/secadmin;
EXECUTE sa_components.create_compartment('XEM_THONG_BAO',1000,'httt','he thong thong tin');
EXECUTE sa_components.create_compartment('XEM_THONG_BAO',2000,'khmt','khoa hoc may tinh');
-- tao group
CONN sec_admin/secadmin;
BEGIN   
sa_components.create_group  
(policy_name => 'XEM_THONG_BAO',
long_name  => 'khoa hoc tu nhien',
short_name  => 'hcmus',
group_num  => 10,
parent_name => NULL); 
END;
CONN sec_admin/secadmin;
EXECUTE sa_components.create_group('XEM_THONG_BAO',20,'it','cntt','hcmus');
EXECUTE sa_components.create_group('XEM_THONG_BAO',30,'vl','vat ly','hcmus');
EXECUTE sa_components.create_group('XEM_THONG_BAO',40,'hh','hoa hoc','hcmus');
-- tao label
CONN sec_admin/secadmin;
BEGIN  
sa_label_admin.drop_label 
(policy_name  => 'XEM_THONG_BAO',
label_value  => 'sv'); 
END;
CONN sec_admin/secadmin;
BEGIN 
sa_label_admin.drop_label
(policy_name  => 'XEM_THONG_BAO',
label_tag  => 50000);
END;
CONN sec_admin/secadmin;
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',10000,'sv');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',10010,'sv:httt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',10020,'sv:khmt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',11020,'sv:httt:it');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',12040,'sv:khmt:it'); 
CONN sec_admin/secadmin;
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',20000,'gv');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',20010,'gv:httt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',20020,'gv:khmt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',21010,'gv:httt:it');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',22020,'gv:khmt:it'); 

CONN sec_admin/secadmin;
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',30000,'tk');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',30010,'tk:httt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',30020,'tk:khmt');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',31010,'tk:httt:it');
EXECUTE sa_label_admin.create_label('XEM_THONG_BAO',32020,'tk:khmt:it');
-- dat level cho user.
CONN ad_sec/adsec;
BEGIN 
  sa_user_admin.set_levels
  (policy_name => 'XEM_THONG_BAO',
  user_name    => 'NVA',
  max_level    => 'sv',
  min_level    => 'sv',
  def_level    => 'sv',
  row_level    => 'sv');
END;
CONN ad_sec/adsec;
BEGIN
sa_user_admin.set_compartments
(policy_name => 'XEM_THONG_BAO',
user_name    => 'NVA',
read_comps   => 'httt',
write_comps  => 'httt',
def_comps    => 'httt',
row_comps    => 'httt'); 
END;
CONN ad_sec/adsec;
BEGIN 
sa_user_admin.set_groups
(policy_name => 'XEM_THONG_BAO',
user_name    => 'NVA',
read_groups  => 'it',
write_groups => 'it',
def_groups   => 'it',
row_groups   => 'it'); 
END;
CONN ad_sec/adsec;
BEGIN 
  sa_user_admin.set_levels
  (policy_name => 'XEM_THONG_BAO',
  user_name    => 'NTB',
  max_level    => 'gv',
  min_level    => 'sv',
  def_level    => 'gv',
  row_level    => 'gv');
END;
CONN ad_sec/adsec;
BEGIN
sa_user_admin.set_compartments
(policy_name => 'XEM_THONG_BAO',
user_name    => 'NTB',
read_comps   => 'httt',
write_comps  => 'httt',
def_comps    => 'httt',
row_comps    => 'httt'); 
END;
CONN ad_sec/adsec;
BEGIN 
sa_user_admin.set_groups
(policy_name => 'XEM_THONG_BAO',
user_name    => 'NTB',
read_groups  => 'it',
write_groups => 'it',
def_groups   => 'it',
row_groups   => 'it'); 
END;

CONN ad_sec/adsec;
BEGIN 
  sa_user_admin.set_levels
  (policy_name => 'XEM_THONG_BAO',
  user_name    => 'LVA',
  max_level    => 'tk',
  min_level    => 'sv',
  def_level    => 'tk',
  row_level    => 'tk');
END;

CONN ad_sec/adsec;
BEGIN
sa_user_admin.set_compartments
(policy_name => 'XEM_THONG_BAO',
user_name    => 'LVA',
read_comps   => 'httt',
write_comps  => 'httt',
def_comps    => 'httt',
row_comps    => 'httt'); 
END;
CONN ad_sec/adsec;
BEGIN 
sa_user_admin.set_groups
(policy_name => 'XEM_THONG_BAO',
user_name    => 'LVA',
read_groups  => 'it',
write_groups => 'it',
def_groups   => 'it',
row_groups   => 'it'); 
END;
 
CONN ad_sec/adsec;
BEGIN 
  sa_user_admin.set_user_privs
  (policy_name  => 'XEM_THONG_BAO',
  user_name     => 'admin',
  PRIVILEGES    => 'FULL');
END;
CONN ad_sec/adsec;
BEGIN 
  sa_user_admin.set_user_privs
  (policy_name  => 'XEM_THONG_BAO',
  user_name     => 'system',
  PRIVILEGES    => 'FULL');
END;
-- gan policy vao bang thong bao
CONN sec_admin/secadmin;
BEGIN 
  sa_policy_admin.apply_table_policy
  (policy_name     => 'XEM_THONG_BAO',
  schema_name      => 'admin',
  table_name       => 'THONG_BAO',
  table_options    => 'READ_CONTROL,WRITE_CONTROL,CHECK_CONTROL'); 
END;

UPDATE admin.thong_bao SET OLS_NUM = char_to_label('XEM_THONG_BAO', 'sv') WHERE NGUOI_THONG_BAO = '1';
UPDATE admin.thong_bao SET OLS_NUM = char_to_label('XEM_THONG_BAO', 'gv') WHERE NGUOI_THONG_BAO = '2';
UPDATE admin.thong_bao SET OLS_NUM = char_to_label('XEM_THONG_BAO', 'tk') WHERE NGUOI_THONG_BAO = '3';


select * from ADMIN.THONG_BAO;


