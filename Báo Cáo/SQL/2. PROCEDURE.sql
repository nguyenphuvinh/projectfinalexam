-- Tao ngu canh people_CV de luu chuc vu cua user dang nhap vao he thong
-- v� tuy vao chuc vu, user se co cac quyen tuong ung
-- trong ham ch�nh sach SEC_FUNCTION cua table PEOPLE se 
-- su dung people_CV de kiem tra chuc vu cua User khi dang nhap

/*B1:  Tao package PEOPLE_CHUCVU_MGR */
create or replace PACKAGE PEOPLE_CHUCVU_MGR AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
    PROCEDURE set_ChucVu; -- tao session luu Chuc_Vu cua user
    PROCEDURE clear_ChucVu;-- xoa session luu Chuc_Vu cua user

END PEOPLE_CHUCVU_MGR;

/*B2: Tao package Body de cai dat ham set_chucVu va clear_ChucVu */ 
CREATE OR REPLACE PACKAGE BODY PEOPLE_CHUCVU_MGR  
    AS  
     PROCEDURE set_ChucVu 
      AS   
        l_ChucVu  NUMBER(2,0);
      BEGIN  
        SELECT Chuc_Vu    INTO l_ChucVu 
         FROM ADMIN.PEOPLE  
        WHERE USERNAME = SYS_CONTEXT ('userenv',
                             'session_user');
       DBMS_SESSION.set_context  
                          (namespace    => 'people_CV',
                           ATTRIBUTE    => 'chucvu',
                           VALUE        => l_ChucVu);
     END set_ChucVu;
  /*--------------------------------------------------*/
     PROCEDURE clear_ChucVu  
     AS  
     BEGIN  
       DBMS_SESSION.clear_context 
                         (namespace    => 'people_CV',
                          ATTRIBUTE    => 'chucvu');
     END clear_ChucVu;  
   END PEOPLE_CHUCVU_MGR;
   
/*B3: Tao ngu canh ten 'people_CV' li�n ket voi package PEOPLE_CHUCVU_MGR d� dinh nghia */  
CREATE CONTEXT people_CV USING sys.PEOPLE_CHUCVU_MGR;

/*B4: Tao trigger de thiet lap ngu canh tu dong moi khi user dang nhap v�o he thong */  
CREATE OR REPLACE TRIGGER set_user_chucvu
      AFTER LOGON ON DATABASE
  BEGIN
    SYS.People_ChucVu_mgr.set_ChucVu;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      NULL;
  END;


